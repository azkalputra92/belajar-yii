<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaPemesananDetail */

$this->title = Yii::t('kvgrid','Add').' Pemesanan Detail ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-pemesanan-detail-create">
    <?= $this->render('_form_pilih', [
        'model' => $model,
    ]) ?>
</div>

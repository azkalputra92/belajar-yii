<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@keuangan', dirname(dirname(__DIR__)) . '/keuangan');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@gudang', dirname(dirname(__DIR__)) . '/gudang');
Yii::setAlias('@penjualan', dirname(dirname(__DIR__)) . '/penjualan');


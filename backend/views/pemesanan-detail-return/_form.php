<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaPemesananDetailReturn */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!Yii::$app->request->isAjax){ ?>
<div class="row">
<div class="col-sm-12">
<div class="card">
<div class="card-body">
<?php } ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_pemesanan')->textInput() ?>

    <?= $form->field($model, 'id_obat')->textInput() ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'id_return')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Menambahkan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    

<?php if (!Yii::$app->request->isAjax){ ?>
</div>
</div>
</div>
</div>
<?php } ?>

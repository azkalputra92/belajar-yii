<?php

namespace common\authCustom;

use Yii;
use OAuth2\Scope;
use OAuth2\Storage\Memory;
use common\models\AuthAssignment;
use yii\helpers\ArrayHelper;

class ScopeHelper {

    public $defaultScope;
    public $supportedScopes;

    public function __construct(string $defaultScope = null, array $supportedScopes = []){
        $this->defaultScope = "";
        $this->supportedScopes = [''];
    }

    public function setScope(){

        $memory = new Memory([
            'default_scope' => $this->defaultScope,
            'supported_scopes' => $this->supportedScopes
        ]);

        $scopeUtil = new Scope($memory);

        return $scopeUtil;

    }
} 
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\TaPemesananDetail;


return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // 'vAlign' => 'middle',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'supplier.supplier',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'tanggal_pesan',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'format' => 'currency',
        'attribute' => 'total_harga',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'statusPemesanan.status',
        'vAlign' => 'middle',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // 'vAlign' => 'middle',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // 'vAlign' => 'middle',
    // ],
  
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'template' => '{detail}',
        'buttons' => [
            "detail" => function ($url, $model, $key) {
                return  Html::a('Detail', ['pemesanan-detail/index-detail', 'id_pemesanan' => $model->id],[
                    'class' => 'btn btn-primary mb-2',
                    'title'=>Yii::t('kvgrid','Detail Pemesanan'), 
                    //'data-pjax' => '0',
                    'role' => 'modal-remote',
                    'target' => 'modal'
                ]);
            },
            // "return" => function ($url, $model, $key) {

            //     return  Html::a('Return', ['proses-selesai', 'id_pemesanan' => $model->id], [
            //         'class' => "btn btn-danger mb-2",
            //         'role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
            //         'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            //         'data-request-method'=>'post',
            //         'data-toggle'=>'tooltip',
            //         'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
            //         'data-confirm-message'=>Yii::t('kvgrid','Apakah barang sudah sampai?') 
            //     ]);
            // }
        ]
    ],  [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
                          'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    ],
];

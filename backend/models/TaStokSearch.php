<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaStok;

/**
 * TaStokSearch represents the model behind the search form about `common\models\TaStok`.
 */
class TaStokSearch extends TaStok
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_obat', 'id_status_stok', 'id_pemesanan', 'created_at', 'updated_at', 'created_by', 'updated_by', 'jumlah'], 'integer'],
            [['tanggal', 'satuan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaStok::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_obat' => $this->id_obat,
            'tanggal' => $this->tanggal,
            'id_status_stok' => $this->id_status_stok,
            'id_pemesanan' => $this->id_pemesanan,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'jumlah' => $this->jumlah,
        ]);

        $query->andFilterWhere(['ilike', 'satuan', $this->satuan]);

        return $dataProvider;
    }
}

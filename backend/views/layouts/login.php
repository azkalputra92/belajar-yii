<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\FontAsset;
use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;


FontAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" style="--theme-deafult: #4831D4; --theme-secondary: #7366ff;" class="h-100">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>

<body class="dark-sidebar">
  <?php $this->beginBody() ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-7"><?= Html::img('@web/images/login/2.jpg', ['class' => 'bg-img-cover bg-center',  'alt'=>"looginpage"]) ?></div>
        <div class="col-xl-5 p-0">
          <div class="login-card">
            <div>
              <div><a class="logo text-start" href="index.html"><?= Html::img('@web/images/logo/login.png', ['class' => 'img-fluid for-ligh',  'alt'=>"looginpage"]) ?></a></div>
              
              <div class="login-main"> 
                  <?= $content ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage();

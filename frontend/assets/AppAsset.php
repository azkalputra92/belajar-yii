<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'temp_assets/css/font-awesome.css',
        'temp_assets/css/vendors/icofont.css',
        'temp_assets/css/vendors/themify.css',
        'temp_assets/css/vendors/flag-icon.css',
        'temp_assets/css/vendors/feather-icon.css',
        'temp_assets/css/vendors/animate.css',
        'temp_assets/css/vendors/owlcarousel.css',
        // 'temp_assets/css/vendors/bootstrap.css',
        'temp_assets/css/style.css',
        'temp_assets/css/color-1.css',
        'temp_assets/css/responsive.css'
    ];
    public $js = [
        // 'temp_assets/js/jquery-3.5.1.min.js',
        // 'temp_assets/js/bootstrap/bootstrap.bundle.min.js',
        'temp_assets/js/icons/feather-icon/feather.min.js',
        'temp_assets/js/icons/feather-icon/feather-icon.js',
        'temp_assets/js/config.js',
        'temp_assets/js/owlcarousel/owl.carousel.js',
        'temp_assets/js/tooltip-init.js',
        'temp_assets/js/animation/wow/wow.min.js',
        'temp_assets/js/landing_sticky.js',
        'temp_assets/js/landing.js',
        'temp_assets/js/script.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}

<?php

namespace backend\controllers;

use Yii;
use common\models\TaStok;
use common\models\TaPemesanan;
use common\models\TaPemesananDetailSampai;
use backend\models\TaStokSearch;
use backend\models\TaPemesananSearch;
use backend\models\TaPemesananDetailSearch;
use backend\models\RefObatSearch;
use common\models\RefObat;
use common\models\TaPemesananDetail;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * StokController implements the CRUD actions for TaStok model.
 */
class StokController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaStok models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new TaStokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Stok  ",
                'content' => $this->renderAjax('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }



    public function actionRiwayat($id = null)
    {
        $request = Yii::$app->request;
        $searchModel = new TaStokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id' => $id]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Stok  ",
                'content' => $this->renderAjax('index_lihat', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_lihat', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }


    public function actionProsesSelesai($id_pemesanan)
    {
        $request = Yii::$app->request;
        $modelPemesanan = TaPemesanan::find()->where(['id' => $id_pemesanan])->one();
        $modelPemesanan->setStatus(100);
        $modelPemesananDetail = TaPemesananDetail::find()->where(['id_pemesanan' => $id_pemesanan])->all();
        foreach ($modelPemesananDetail as $pemesananDetail) {
            $model = new TaStok();
            $model->id_obat = $pemesananDetail->id_obat;
            $model->id_status_stok = 1;
            $model->id_pemesanan = $id_pemesanan;
            $model->tanggal = date('Y-m-d');
            $model->jumlah = ($pemesananDetail->taPemesananDetailReturn) ? $pemesananDetail->jumlah - $pemesananDetail->taPemesananDetailReturn->jumlah : $pemesananDetail->jumlah;
            $model->setMasuk($pemesananDetail->id_obat, $model->jumlah);
            $model->save();
            $TaPemesananDetailSampai = new TaPemesananDetailSampai;
            $TaPemesananDetailSampai->id_pemesanan = $model->jumlah;
            $TaPemesananDetailSampai->id_obat = $model->id_obat;
            $TaPemesananDetailSampai->jumlah = $model->jumlah;
            $TaPemesananDetailSampai->save();
        }
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pemesanan-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }


    /**
     * Displays a single TaStok model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => Yii::t('kvgrid', 'View') . " Stok  ",
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('kvgrid', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionPilihHilang()
    {
        $request = Yii::$app->request;
        $searchModel = new RefObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['not', ['stok' => null]]); //mencari nilai stok yang is not null
        //$dataProvider->query->andFilterWhere(['stok' => !null]);
        //->andWhere(['not', ['info' => null]]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan  ",
                'content' => $this->renderAjax('index_pilih_hilang', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_pilih_hilang', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    public function actionPilihRusak()
    {
        $request = Yii::$app->request;
        $searchModel = new RefObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['not', ['stok' => null]]); //mencari nilai stok yang is not null
        //$dataProvider->query->andFilterWhere(['stok' => !null]);
        //->andWhere(['not', ['info' => null]]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan  ",
                'content' => $this->renderAjax('index_pilih_obat', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_pilih_obat', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }


    public function actionHilang()
    {
        $request = Yii::$app->request;
        $searchModel = new TastokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status_stok' => 5]);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan  ",
                'content' => $this->renderAjax('index_hilang', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_hilang', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    public function actionPilihExpired()
    {
        $request = Yii::$app->request;
        $searchModel = new RefObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['not', ['stok' => null]]); //mencari nilai stok yang is not null
        //$dataProvider->query->andFilterWhere(['stok' => !null]);
        //->andWhere(['not', ['info' => null]]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan  ",
                'content' => $this->renderAjax('index_pilih_expired', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_pilih_expired', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    public function actionRusak()
    {
        $request = Yii::$app->request;
        $searchModel = new TastokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status_stok' => 3]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan  ",
                'content' => $this->renderAjax('index_rusak', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_rusak', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    public function actionExpired()
    {
        $request = Yii::$app->request;
        $searchModel = new TastokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status_stok' => 4]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan  ",
                'content' => $this->renderAjax('index_expired', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_expired', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Creates a new TaStok model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TaStok();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Stok ",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => Yii::t('kvgrid', 'Create New') . " Stok ",
                    'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' TaStok ' . Yii::t('kvgrid', 'success') . '</span>',
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::a(Yii::t('kvgrid', 'Create More'), ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Stok ",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing TaStok model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreateRusak($id)
    {
        $request = Yii::$app->request;
        $modelObat =  RefObat::findOne($id);
        $model = new TaStok();




        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {


                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " data rusak ",
                    'content' => $this->renderAjax('create_rusak', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                if ($model->jumlah < $modelObat->stok) {


                    $model->setStatus(3, $id);
                    $modelObat->stok = $modelObat->stok - $model->jumlah;
                    $modelObat->save();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => Yii::t('kvgrid', 'Create New') . "  data rusak  ",
                        'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' data rusak ' . Yii::t('kvgrid', 'success') . '</span>',
                        'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                        // Html::a(Yii::t('kvgrid', 'Create More'), ['create_rusak', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => Yii::t('kvgrid', 'Create New') . "  Data Stok Rusak Gagal  ",
                        'content' => '<span class="text-danger">' . 'Data Stok yang anda masukkan lebih dari jumlah stok yang ada!' . '</span>',
                        'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                        // Html::a(Yii::t('kvgrid', 'Create More'), ['create_rusak', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                }
            } else {

                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Stok ",
                    'content' => $this->renderAjax('create_rusak', [
                        'model' => $model,
                        // 'modelStok' => $modelStok,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                // $model->stok = $model->stok - $model->jumlah;
                // $model->save();
                // $model->setStatus(3, $id);
                $model->setStatus(3, $id);

                $modelObat->stok = $modelObat->stok - $model->jumlah;
                $modelObat->save();

                return $this->redirect(['create_rusak', 'id' => $model->id]);
            } else {
                return $this->render('create_rusak', [
                    'model' => $model,
                    // 'modelStok' => $modelStok,
                ]);
            }
        }
    }


    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $TaStok = TaStok::findOne(['id' => $id]);
        $modelObat =  RefObat::findOne(['id' => $TaStok->id_obat]);
        $model = $this->findModel($id);
        $model->jumlah_rusak = $model->jumlah;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Update') . " Status Penjualan  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->jumlah = $model->jumlah_rusak;
                $model->save();
                $modelObat->stok += $TaStok->jumlah;
                $modelObat->save();
                $modelObat->stok = $modelObat->stok - $model->jumlah;
                $modelObat->save();

                return [

                    'forceReload' => '#crud-datatable-pjax',
                    'title' =>   " Mengubah  data rusak  ",
                    'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Update') . ' data rusak ' . Yii::t('kvgrid', 'success') . '</span>',
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'View') . " Status Penjualan  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {
                $model->jumlah = $model->jumlah_rusak;
                $model->save();
                $modelObat->stok += $TaStok->jumlah;
                $modelObat->save();
                $modelObat->stok = $modelObat->stok - $model->jumlah;
                $modelObat->save();

                return [

                    'forceReload' => '#crud-datatable-pjax',
                    'title' =>   " Mengubah  data rusak  ",
                    'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Update') . ' data rusak ' . Yii::t('kvgrid', 'success') . '</span>',
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }
    public function actionCreateExpired($id)
    {
        $request = Yii::$app->request;
        $modelObat =  RefObat::findOne($id);
        $model = new TaStok();




        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {


                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " data expired ",
                    'content' => $this->renderAjax('create_expired', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                if ($model->jumlah < $modelObat->stok) {
                    $model->setStatus(4, $id);
                    $modelObat->stok = $modelObat->stok - $model->jumlah;
                    $modelObat->save();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => Yii::t('kvgrid', 'Create New') . "  data expired  ",
                        'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' data expired ' . Yii::t('kvgrid', 'success') . '</span>',
                        'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                        // Html::a(Yii::t('kvgrid', 'Create More'), ['create_expired', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => Yii::t('kvgrid', 'Create New') . "  Data Stok expired Gagal  ",
                        'content' => '<span class="text-danger">' . 'Data Stok yang anda masukkan lebih dari jumlah stok yang ada!' . '</span>',
                        'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                        // Html::a(Yii::t('kvgrid', 'Create More'), ['create_expired', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                }
            } else {

                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Stok ",
                    'content' => $this->renderAjax('create_expired', [
                        'model' => $model,
                        // 'modelStok' => $modelStok,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate() && $model->save()) {
                // $model->stok = $model->stok - $model->jumlah;
                // $model->save();
                // $model->setStatus(3, $id);
                $model->setStatus(3, $id);

                $modelObat->stok = $modelObat->stok - $model->jumlah;
                $modelObat->save();

                return $this->redirect(['create_expired', 'id' => $model->id]);
            } else {
                return $this->render('create_expired', [
                    'model' => $model,
                    // 'modelStok' => $modelStok,
                ]);
            }
        }
    }
    public function actionCreateHilang($id)
    {
        $request = Yii::$app->request;
        $modelObat =  RefObat::findOne($id);
        $model = new TaStok();




        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {


                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " data hilang ",
                    'content' => $this->renderAjax('create_hilang', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                if ($model->jumlah < $modelObat->stok) {


                    $model->setStatus(5, $id);
                    $modelObat->stok = $modelObat->stok - $model->jumlah;
                    $modelObat->save();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => Yii::t('kvgrid', 'Create New') . "  data hilang  ",
                        'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' data hilang ' . Yii::t('kvgrid', 'success') . '</span>',
                        'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                        // Html::a(Yii::t('kvgrid', 'Create More'), ['create_hilang', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => Yii::t('kvgrid', 'Create New') . "  Data Stok hilang Gagal  ",
                        'content' => '<span class="text-danger">' . 'Data Stok yang anda masukkan lebih dari jumlah stok yang ada!' . '</span>',
                        'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                        // Html::a(Yii::t('kvgrid', 'Create More'), ['create_hilang', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                }
            } else {

                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Stok ",
                    'content' => $this->renderAjax('create_hilang', [
                        'model' => $model,
                        // 'modelStok' => $modelStok,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                // $model->stok = $model->stok - $model->jumlah;
                // $model->save();
                // $model->setStatus(3, $id);
                $model->setStatus(5, $id);

                $modelObat->stok = $modelObat->stok - $model->jumlah;
                $modelObat->save();

                return $this->redirect(['create_hilang', 'id' => $model->id]);
            } else {
                return $this->render('create_hilang', [
                    'model' => $model,
                    // 'modelStok' => $modelStok,
                ]);
            }
        }
    }

    /**
     * Delete an existing TaStok model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $request = Yii::$app->request;
        // $this->findModel($id)->delete();

        // if ($request->isAjax) {
        //     /*
        //     *   Process for ajax request
        //     */
        //     Yii::$app->response->format = Response::FORMAT_JSON;
        //     return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        // } else {
        //     /*
        //     *   Process for non-ajax request
        //     */
        //     return $this->redirect(['index']);
        // }

        $request = Yii::$app->request;
        $thisModel = $this->findModel($id);
        $modelObat = RefObat::findOne(['id' => $thisModel->id_obat]);
        // $penjualanModel = TaPenjualan::findOne(['id' => $thisModel->id_penjualan]);
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $modelObat->stok += $thisModel->jumlah;
            $modelObat->save();
            // $penjualanModel->save();
            $transaction->commit();
            $thisModel->delete();
            //   $thisModel->setTotalPenjualan();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index_rusak']);
        }
    }

    /**
     * Delete multiple existing TaStok model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the TaStok model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaStok the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaStok::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

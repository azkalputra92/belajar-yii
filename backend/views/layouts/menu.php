<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
?>

<ul class="sidebar-links" id="simple-bar">
    <li class="back-btn">
        <a href="<?= Url::home() ?>">
            <?= Html::img('@web/images/logo/logo-icon.png', ['class' => 'img-fluid']) ?>
        </a>
        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
    </li>
    <li class="sidebar-main-title">
        <div>
            <h6 class="lan-1">Umum</h6>
            <!-- <p class="lan-2">Dashboards,widgets & layout.</p> -->
        </div>
    </li>
    <i class="icofont icofont-doctor-alt"></i>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title link-nav" href="<?= Url::home() ?>"><i data-feather="airplay"> </i><span>Dashboard</span></a></li>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title link-nav" href="<?= Url::toRoute('/obat') ?>"><i data-feather="list"> </i><span>Data Obat</span></a></li>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title link-nav" href="<?= Url::toRoute('/obat-kategori') ?>"><i data-feather="list"> </i><span>Kategori Obat</span></a></li>
    <!-- <li class="sidebar-list"><a class="sidebar-link sidebar-title link-nav" href="<?= Url::toRoute('/pemesanan') ?>"><i data-feather="shopping-cart"> </i><span>Pemesanan</span></a></li> -->
    <li class="sidebar-list"><a class="sidebar-link sidebar-title link-nav" href="<?= Url::toRoute('/supplier') ?>"><i data-feather="list"> </i><span>Supplier</span></a></li>

    <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="sun"></i><span>Pemesanan</span></a>
        <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('pemesanan/index'); ?>">Pesan</a></li>
            <li><a href="<?= Url::toRoute('pemesanan/index-proses'); ?>">Proses</a></li>
            <li><a href="<?= Url::toRoute('pemesanan/index-sampai'); ?>">Sampai</a></li>
            <li><a href="<?= Url::toRoute('pemesanan/index-selesai'); ?>">Selesai</a></li>
        </ul>
    </li>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="sun"></i><span>Penjualan</span></a>
        <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('penjualan/index'); ?>">Pesan</a></li>
            <li><a href="<?= Url::toRoute('penjualan/index-disiapkan'); ?>">Disiapkan</a></li>
            <li><a href="<?= Url::toRoute('penjualan/index-batal'); ?>">Batal</a></li>
            <li><a href="<?= Url::toRoute('penjualan/index-selesai'); ?>">Selesai</a></li>
        </ul> 
    </li>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="sun"></i><span>return</span></a>
    <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('return/index'); ?>">Pesan</a></li>
            <li><a href="<?= Url::toRoute(['return/index','status'=>2]); ?>">Proses</a></li>
            <li><a href="<?= Url::toRoute(['return/index','status'=>3]); ?>">Sampai</a></li>
            <li><a href="<?= Url::toRoute(['return/index','status'=>100]); ?>">Selesai</a></li>
        </ul>
    </li>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="sun"></i><span>Update Stok</span></a>
        <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('stok/rusak'); ?>">Rusak</a></li>
            <li><a href="<?= Url::toRoute('stok/expired'); ?>">Expired</a></li>
            <li><a href="<?= Url::toRoute('stok/hilang'); ?>">Hilang</a></li>
        </ul>
    </li>
   
    <!-- <li class="sidebar-list"><a class="sidebar-link sidebar-title link-nav" href="<?= Url::toRoute('pasien/home'); ?>"><i data-feather="airplay"> </i><span>Dashboard</span></a></li> -->

    <!-- <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="user"></i><span>Dokter</span></a>
        <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('dokter/create'); ?>">Input Data Dokter </a></li>
            <li><a href="<?= Url::toRoute('jadwal-dokter/dokter'); ?>">Jadwal Dokter</a></li>
            <li><a href="<?= Url::toRoute('dokter/index'); ?>">Data dokter</a></li>
        </ul>
    </li>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="book-open"></i><span>Konsultasi</span></a>
        <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('pasien/konsultasi'); ?>">Input Data konsultasi </a></li>
           <li><a href="<?= Url::toRoute('jadwal-konsultasi/index'); ?>">Lihat konsultasi</a></li> -->
    <!-- <li><a href="<?= Url::toRoute('konsultasi/index'); ?>">Data konsultasi</a></li>
</ul>
</li>
<li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="sun"></i><span>perawatan</span></a>
    <ul class="sidebar-submenu">
        <li><a href="<?= Url::toRoute('jenis-penanganan/create'); ?>">Input Data perawatan </a></li>
         <li><a href="<?= Url::toRoute('jadwal-perawatan/index'); ?>">Kategori perawatan</a></li> -->
    <!-- <li><a href="<?= Url::toRoute('jenis-penanganan/index'); ?>">Data perawatan</a></li>
    </ul>
</li>
<li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="sun"></i><span>Referensi</span></a>
    <ul class="sidebar-submenu">
        <li><a href="<?= Url::toRoute('event/index'); ?>">Event </a></li>
        <li><a href="<?= Url::toRoute('diskon/index'); ?>">Diskon</a></li>
        <li><a href="<?= Url::toRoute('pasien-poin/pasien'); ?>">Poin</a></li>
    </ul>
</li>
<li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="users"></i><span>Perawat</span></a>
    <ul class="sidebar-submenu">
        <li><a href="<?= Url::toRoute('perawat/create'); ?>">Input Data Perawat </a></li>
        <li><a href="<?= Url::toRoute('perawat/index'); ?>">Data Perawat</a></li>
    </ul>
</li>  -->
    <!-- <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="tag"></i><span>produk</span></a>
        <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('produk/create'); ?>">Input Data produk </a></li>
            <li><a href="<?= Url::toRoute('jadwal-produk/index'); ?>">Kategori produk</a></li>
            <li><a href="<?= Url::toRoute('produk/index'); ?>">Data produk</a></li>
        </ul>
    </li> -->
    <!-- <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i class="icofont icofont-doctor-alt"></i><span>antrian</span></a>
        <ul class="sidebar-submenu">
            <li><a href="<?= Url::toRoute('antrian/create'); ?>">Input Data antrian </a></li>
            <li><a href="<?= Url::toRoute('jadwal-antrian/index'); ?>">Kategori antrian</a></li>
            <li><a href="<?= Url::toRoute('antrian/index'); ?>">Data antrian</a></li>
        </ul>
    </li> -->
    <!-- <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="airplay"></i><span class="lan-6">Widgets</span></a>
        <ul class="sidebar-submenu">
            <li><a href="general-widget.html">General</a></li>
            <li><a href="chart-widget.html">Chart</a></li>
        </ul>
    </li>
    <li class="sidebar-list"><a class="sidebar-link sidebar-title" href="#"><i data-feather="layout"></i><span class="lan-7">Page layout</span></a>
        <ul class="sidebar-submenu">
            <li><a href="box-layout.html">Boxed</a></li>
            <li><a href="layout-rtl.html">RTL</a></li>
            <li><a href="layout-dark.html">Dark Layout</a></li>
            <li><a href="hide-on-scroll.html">Hide Nav Scroll</a></li>
            <li><a href="footer-light.html">Footer Light</a></li>
            <li><a href="footer-dark.html">Footer Dark</a></li>
            <li><a href="footer-fixed.html">Footer Fixed</a></li>
        </ul>
    </li> -->
</ul>
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RefStatusPenjualan */

$this->title = Yii::t('kvgrid','Add').' Status Penjualan ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ref-status-penjualan-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

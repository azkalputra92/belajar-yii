<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaPemesananDetail */

$this->title = Yii::t('kvgrid','Update').' Pemesanan Detail ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-pemesanan-detail-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaPemesananDetailReturn */

$this->title = Yii::t('kvgrid','Update').' Pemesanan Detail Return ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-pemesanan-detail-return-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

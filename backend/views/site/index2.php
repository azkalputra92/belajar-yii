<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use kartik\grid\GridView;
// use johnitvn\ajaxcrud\CrudAsset; 
use backend\assets\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\icons\Icon;
    
Icon::map($this, Icon::FAS);
CrudAsset::register($this);
$this->registerJsFile(
    '@web/temp_assets/js/modal_large.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->title = 'Dashboard';
?>
<div class="row second-chart-list third-news-update">
    <div class="col-sm-6 col-xl-3 col-lg-6">
        <a href="<?= Url::toRoute(['antrian/pilih-pasien']); ?>" role="modal-remote" title="Tambah Antrian">
            <div class="card o-hidden">
                <div class="bg-success b-r-4 card-body">
                    <div class="media static-top-widget">
                        <div class="align-self-center text-center"><i class="fas fa-plus"></i></div>
                        <div class="media-body"><span class="m-0">Tambah</span>
                            <h4 class="mb-0 counter">Antrian</h4>
                            <i class="fas fa-plus icon-bg"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-xl-3 col-lg-6">
        <a href="<?= Url::toRoute(['antrian/pilih-tanggal']); ?>" role="modal-remote" title="Tambah Antrian">
            <div class="card o-hidden">
                <div class="bg-info b-r-4 card-body">
                    <div class="media static-top-widget">
                        <div class="align-self-center text-center"><i class="fas fa-plus"></i></div>
                        <div class="media-body"><span class="m-0">Tambah</span>
                            <h4 class="mb-0 counter">Antrian Mundur</h4>
                            <i class="fas fa-plus icon-bg"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-xl-3 col-lg-6">
        <div class="card o-hidden">
            <div class="bg-secondary b-r-4 card-body">
            <div class="media static-top-widget">
                <div class="align-self-center text-center"><i class="fas fa-shopping-basket"></i></div>
                <div class="media-body"><span class="m-0">Beli</span>
                <h4 class="mb-0 counter">Obat</h4>
                <i class="fas fa-shopping-basket icon-bg"></i>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Daftar Antrian <?= $tanggal ?></h5>
            </div>
            <div class="card-body">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns_antrian.php'),
                        'toolbar'=> [
                            ['content'=>
                                Html::a('<i class="fas fa-sync-alt"></i> '.Yii::t('kvgrid','Refresh'), [''],
                                ['data-pjax'=>1, 'class'=>'btn btn-light', 'title'=>Yii::t('kvgrid','Reset Grid')])
                                // .'{toggleData}'
                                // .'{export}'
                            ],
                        ],          
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,          
                        'panel' => false
                        // 'panel' => [
                        //     // 'type' => 'primary', 
                        //     // 'heading' => '<i class="fas fa-list"></i> Data Antrian ',
                        //     'before'=>Html::a('<i class="fas fa-plus"></i> '.Yii::t('kvgrid','Add'), ['create'],
                        //                 ['role'=>'modal-remote','title'=> Yii::t('kvgrid','Create New Item'),'class'=>'btn btn-primary']),
                        //     'after'=>'<div class="clearfix"></div>',
                        // ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
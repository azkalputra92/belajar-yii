<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // 'vAlign' => 'middle',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'id_pemesanan',
    //     'vAlign' => 'middle',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Ohat',
        'attribute'=>'refObat.obat',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'jumlah',
        'vAlign' => 'middle',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'id_return',
    //     'vAlign' => 'middle',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'template' => '{detail}',
        'buttons' => [
            "detail" => function ($url, $model, $key) {
                return  Html::a('Pilih', ['pilih', 'id' => $model->id], [
                    'class' => 'btn btn-primary mb-2',
                    'title'=>Yii::t('kvgrid','Pilih'), 
                    'role' => 'modal-remote',
                    'data-confirm'=>false, 'data-method'=>false,
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>Yii::t('kvgrid','Ingin Return ini?'),
                    'data-confirm-message'=>Yii::t('kvgrid','Ingin Return Ini?'),
                ]);
            },
        ],
    ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'id'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
    //                       'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    // ],

];   
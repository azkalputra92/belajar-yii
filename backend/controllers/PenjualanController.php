<?php

namespace backend\controllers;

use Yii;
use common\models\TaPenjualan;
use backend\models\TaPenjualanSearch as TaPenjualanSearch;
use backend\models\TaPenjualanDetailSearch;
use common\models\TaStok;
use common\models\RefObat;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * PenjualanController implements the CRUD actions for TaPenjualan model.
 */
class PenjualanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaPenjualan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new TaPenjualanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status' => 1]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Penjualan  ",
                'content' => $this->renderAjax('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexDisiapkan()
    {
        $request = Yii::$app->request;
        $searchModel = new TaPenjualanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status' => 2]);


        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Penjualan  ",
                'content' => $this->renderAjax('index-disiapkan', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index-disiapkan', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexBatal()
    {
        $request = Yii::$app->request;
        $searchModel = new TaPenjualanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status' => 99]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Penjualan  ",
                'content' => $this->renderAjax('index-batal', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index-batal', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexSelesai()
    {
        $request = Yii::$app->request;
        $searchModel = new TaPenjualanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status' => 100]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Penjualan  ",
                'content' => $this->renderAjax('index-selesai', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index-selesai', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionSiapkan($id_penjualan)
    {
        $request = Yii::$app->request;
        $model = TaPenjualan::find()->where(['id' => $id_penjualan])->one();
        $model->setStatus(2);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-penjualan-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBatalkan($id_penjualan)
    {
        $request = Yii::$app->request;
        $model = TaPenjualan::find()->where(['id' => $id_penjualan])->one();
        $penjDetailModels = TaPenjualanDetailSearch::findAll(['id_penjualan' => $id_penjualan]);

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            foreach ($penjDetailModels as $penjDetail) {
                $obatModel = RefObat::findOne(['id' => $penjDetail->id_obat]);
                $obatModel->stok += $penjDetail->jumlah;
                $obatModel->save();
            }

            if ($model->save()) {
                $model->setStatus(99);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-penjualan-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionSelesaikan($id_penjualan)
    {
        $request = Yii::$app->request;
        $model = TaPenjualan::find()->where(['id' => $id_penjualan])->one();
        $penDetailModels = TaPenjualanDetailSearch::findAll(['id_penjualan' => $id_penjualan]);


        /**
         * SEHARUSNYA STOK DI HAPUS DISINI
         * 
         */


        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            foreach ($penDetailModels as $penDetail) {
                $stokModel = new TaStok();
                $stokModel->id_status_stok = 2;
                $stokModel->id_obat = $penDetail->id_obat;
                $stokModel->id_pemesanan = $id_penjualan;
                $stokModel->jumlah = $penDetail->jumlah;
                $stokModel->tanggal = date('Y-m-d');
                $stokModel->save();
            }
            if ($model->save()) {
                $model->setStatus(100);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }


        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-penjualan-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Displays a single TaPenjualan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => Yii::t('kvgrid', 'View') . " Penjualan  ",
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('kvgrid', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TaPenjualan model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TaPenjualan();
        $model->tanggal = date("Y-m-d");
        $model->no_invoice = "INV-" . date("Ymd") . "-" . random_int(0, 1000);
        $model->total_harga = 0;

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            if ($model->save()) {
                $model->setStatus(1);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }


        return $this->redirect(['index']);
    }
    /**
     * Updates an existing TaPenjualan model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Update') . " Penjualan  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-penjualan-pjax',
                    'title' => Yii::t('kvgrid', 'View') . " Penjualan  ",
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::a(Yii::t('kvgrid', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'View') . " Penjualan  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing TaPenjualan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $penjDetailModels = TaPenjualanDetailSearch::findAll(['id_penjualan' => $id]);

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            foreach ($penjDetailModels as $penjDetail) {
                $obatModel = RefObat::findOne(['id' => $penjDetail->id_obat]);
                $obatModel->stok += $penjDetail->jumlah;
                $obatModel->save();
                $penjDetail->delete();
            }

            $this->findModel($id)->delete();
            // $
            // if($model->save()){
            //     $model->setStatus(1);
            // }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-penjualan-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing TaPenjualan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the TaPenjualan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaPenjualan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaPenjualan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\TaPemesananDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!Yii::$app->request->isAjax) { ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                <?php } ?>

                <?php $form = ActiveForm::begin(); ?>

                <?php // $form->field($model, 'id_pemesanan')->textInput() 
                ?>

                <?php //$form->field($model, 'id_obat')->textInput() 
                ?>

                <?php //$form->field($model, 'harga')->textInput() 
                ?>

                <?php
                // Integer only
                echo '<label class="control-label">Harga</label>';
                echo NumberControl::widget([
                    'model' => $model,
                    'attribute' => 'harga',
                    'maskedInputOptions' => ['digits' => 0, 'prefix' => 'Rp ', 'groupSeparator' => '.', 'radixPoint' => ','],
                ]);
                ?>

                <?= $form->field($model, 'jumlah')->textInput() ?>

                <?php //$form->field($model, 'total')->textInput() 
                ?>


                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Menambahkan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>


                <?php if (!Yii::$app->request->isAjax) { ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php
namespace common\components;

use Yii;
use yii\base\Component;
use common\models\RefDokter;

class PenggunaComponent extends Component {

    public function getDokter(){
        $id_user = Yii::$app->user->identity->id;
        return RefDokter::find()->where(['id_user' => $id_user])->one();
    }
}

?>
<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // 'vAlign' => 'middle',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'filterType'=> \kartik\grid\GridView::FILTER_DATE, 
    //     'filterWidgetOptions' => [
    //         'options' => ['placeholder' => 'Pilih Tanggal'],
    //         'pluginOptions' => [
    //             'format' => 'yyyy-m-dd',
    //             'todayHighlight' => true
    //         ]
    //     ],
    //     'attribute'=>'tanggal',
    //     'vAlign' => 'middle',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        // 'header' => 'Jenis',
        'attribute'=>'no_antrian',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header' => 'Jenis',
        'attribute'=>'jenisAntrian.jenis_antrian',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header' => 'Pasien',
        'attribute'=>'pasien.nama',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header' => 'Dokter',
        'attribute'=>'dokter.nama',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dokterJadwal.jam_masuk',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'penanganan.penanganan.jenis_penanganan',
        'vAlign' => 'middle',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'vAlign'=>'middle',
        'template' => '{hadir}{cancel}',
        'buttons' => [
            "hadir" => function ($url, $model, $key){
                if($model->id_tahap == 1){
                    return Html::a('Hadir', ['antrian/proses-hadir','id_antrian' => $model->id,'id_pasien' => $model->id_pasien], 
                    [
                        'class' =>'btn btn-primary btn-block block-button', 
                        'role'=>'modal-remote',
                        'title'=>'Proses Konsultasi',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
                        'data-confirm-message'=>Yii::t('kvgrid','Anda yakin pasien hadir?')
                    ]);
                }
            }, 
            "cancel" => function ($url, $model, $key){
                if($model->id_tahap == 1){
                    return Html::a('Cancel', ['antrian/proses-cancel','id_antrian' => $model->id,'id_pasien' => $model->id_pasien], 
                    [
                        'class' =>'btn btn-danger btn-block block-button', 
                        'role'=>'modal-remote',
                        'title'=>'Proses Konsultasi',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
                        'data-confirm-message'=>Yii::t('kvgrid','Anda yakin pasien batal?')
                    ]);
                }
            }, 
        ]
    ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'id'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
    //                       'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    // ],

];   
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaPenjualan */

$this->title = Yii::t('kvgrid','Update').' Penjualan ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-penjualan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

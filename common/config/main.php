<?php
return [
    // 'language' => 'id-ID',
    'language' => 'id',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                // 'frontend*' => [
                //     'class' => 'yii\i18n\PhpMessageSource',
                //     'basePath' => '@common/messages',
                // ],
                // 'backend*' => [
                //     'class' => 'yii\i18n\PhpMessageSource',
                //     'basePath' => '@common/messages',
                // ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'kvgrid*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'pengguna' => [
            'class' => 'common\components\PenggunaComponent',
        ],
        'formatter' => [
            'class' => 'yii\i18n\formatter',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
            'currencyCode' => 'Rp. '
        ],
    ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'generators' => [
                'crudTemplate'   => [
                    'class' => 'common\costumgenerators\Generator',
                ],
                'crud-api'   => [
                    'class' => 'common\generators\Generator',
                ],
            ]
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
        ]
    ],
];

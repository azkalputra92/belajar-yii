<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RefSupplierObat */

$this->title = Yii::t('kvgrid','Add').' Supplier Obat ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ref-supplier-obat-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

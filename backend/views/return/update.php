<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaReturn */

$this->title = Yii::t('kvgrid','Update').' Return ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-return-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

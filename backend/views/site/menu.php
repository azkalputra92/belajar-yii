<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Dashboard';
?>
<!-- <div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                <?php //echo \Yii::t('app', 'Application'); 
                ?>
            </div>
        </div>
    </div>
</div> -->

<div class="row second-chart-list third-news-update">

    <div class="col-xl-9 xl-100 chart_data_left box-col-12">
        <div class="card">
            <div class="card-body p-0">
                <div class="row m-0 chart-main">
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                        <div class="media align-items-center">
                            <div class="hospital-small-chart">
                                <div class="small-bar">
                                    <div class="small-chart flot-chart-container">
                                        <div class="chartist-tooltip"></div><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-bar" style="width: 100%; height: 100%;">
                                            <g class="ct-grids"></g>
                                            <g>
                                                <g class="ct-series ct-series-a">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="69" y2="58.2" class="ct-bar" ct:value="400" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="69" y2="44.7" class="ct-bar" ct:value="900" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="69" y2="47.4" class="ct-bar" ct:value="800" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="69" y2="42" class="ct-bar" ct:value="1000" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="69" y2="50.1" class="ct-bar" ct:value="700" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="69" y2="36.6" class="ct-bar" ct:value="1200" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="69" y2="60.9" class="ct-bar" ct:value="300" style="stroke-width: 3px"></line>
                                                </g>
                                                <g class="ct-series ct-series-b">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="58.2" y2="31.200000000000003" class="ct-bar" ct:value="1000" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="44.7" y2="31.200000000000003" class="ct-bar" ct:value="500" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="47.4" y2="31.199999999999996" class="ct-bar" ct:value="600" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="42" y2="31.200000000000003" class="ct-bar" ct:value="400" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="50.1" y2="31.200000000000003" class="ct-bar" ct:value="700" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="36.6" y2="31.200000000000003" class="ct-bar" ct:value="200" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="60.9" y2="31.199999999999996" class="ct-bar" ct:value="1100" style="stroke-width: 3px"></line>
                                                </g>
                                            </g>
                                            <g class="ct-labels"></g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="right-chart-content">
                                    <h4>1001</h4><span>Total Pasien </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                        <div class="media align-items-center">
                            <div class="hospital-small-chart">
                                <div class="small-bar">
                                    <div class="small-chart1 flot-chart-container">
                                        <div class="chartist-tooltip"></div><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-bar" style="width: 100%; height: 100%;">
                                            <g class="ct-grids"></g>
                                            <g>
                                                <g class="ct-series ct-series-a">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="69" y2="58.2" class="ct-bar" ct:value="400" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="69" y2="52.8" class="ct-bar" ct:value="600" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="69" y2="44.7" class="ct-bar" ct:value="900" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="69" y2="47.4" class="ct-bar" ct:value="800" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="69" y2="42" class="ct-bar" ct:value="1000" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="69" y2="36.6" class="ct-bar" ct:value="1200" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="69" y2="55.5" class="ct-bar" ct:value="500" style="stroke-width: 3px"></line>
                                                </g>
                                                <g class="ct-series ct-series-b">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="58.2" y2="31.200000000000003" class="ct-bar" ct:value="1000" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="52.8" y2="31.199999999999996" class="ct-bar" ct:value="800" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="44.7" y2="31.200000000000003" class="ct-bar" ct:value="500" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="47.4" y2="31.199999999999996" class="ct-bar" ct:value="600" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="42" y2="31.200000000000003" class="ct-bar" ct:value="400" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="36.6" y2="31.200000000000003" class="ct-bar" ct:value="200" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="55.5" y2="31.200000000000003" class="ct-bar" ct:value="900" style="stroke-width: 3px"></line>
                                                </g>
                                            </g>
                                            <g class="ct-labels"></g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="right-chart-content">
                                    <h4>1005</h4><span>Total Penjualan</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                        <div class="media align-items-center">
                            <div class="hospital-small-chart">
                                <div class="small-bar">
                                    <div class="small-chart2 flot-chart-container">
                                        <div class="chartist-tooltip"></div><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-bar" style="width: 100%; height: 100%;">
                                            <g class="ct-grids"></g>
                                            <g>
                                                <g class="ct-series ct-series-a">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="69" y2="39.3" class="ct-bar" ct:value="1100" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="69" y2="44.7" class="ct-bar" ct:value="900" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="69" y2="52.8" class="ct-bar" ct:value="600" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="69" y2="42" class="ct-bar" ct:value="1000" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="69" y2="50.1" class="ct-bar" ct:value="700" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="69" y2="36.6" class="ct-bar" ct:value="1200" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="69" y2="60.9" class="ct-bar" ct:value="300" style="stroke-width: 3px"></line>
                                                </g>
                                                <g class="ct-series ct-series-b">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="39.3" y2="31.199999999999996" class="ct-bar" ct:value="300" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="44.7" y2="31.200000000000003" class="ct-bar" ct:value="500" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="52.8" y2="31.199999999999996" class="ct-bar" ct:value="800" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="42" y2="31.200000000000003" class="ct-bar" ct:value="400" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="50.1" y2="31.200000000000003" class="ct-bar" ct:value="700" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="36.6" y2="31.200000000000003" class="ct-bar" ct:value="200" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="60.9" y2="31.199999999999996" class="ct-bar" ct:value="1100" style="stroke-width: 3px"></line>
                                                </g>
                                            </g>
                                            <g class="ct-labels"></g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="right-chart-content">
                                    <h4>100</h4><span>Total Kunjungan</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                        <div class="media border-none align-items-center">
                            <div class="hospital-small-chart">
                                <div class="small-bar">
                                    <div class="small-chart3 flot-chart-container">
                                        <div class="chartist-tooltip"></div><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-bar" style="width: 100%; height: 100%;">
                                            <g class="ct-grids"></g>
                                            <g>
                                                <g class="ct-series ct-series-a">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="69" y2="58.2" class="ct-bar" ct:value="400" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="69" y2="52.8" class="ct-bar" ct:value="600" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="69" y2="47.4" class="ct-bar" ct:value="800" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="69" y2="42" class="ct-bar" ct:value="1000" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="69" y2="50.1" class="ct-bar" ct:value="700" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="69" y2="39.3" class="ct-bar" ct:value="1100" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="69" y2="60.9" class="ct-bar" ct:value="300" style="stroke-width: 3px"></line>
                                                </g>
                                                <g class="ct-series ct-series-b">
                                                    <line x1="13.571428571428571" x2="13.571428571428571" y1="58.2" y2="31.200000000000003" class="ct-bar" ct:value="1000" style="stroke-width: 3px"></line>
                                                    <line x1="20.714285714285715" x2="20.714285714285715" y1="52.8" y2="39.3" class="ct-bar" ct:value="500" style="stroke-width: 3px"></line>
                                                    <line x1="27.857142857142858" x2="27.857142857142858" y1="47.4" y2="31.199999999999996" class="ct-bar" ct:value="600" style="stroke-width: 3px"></line>
                                                    <line x1="35" x2="35" y1="42" y2="33.9" class="ct-bar" ct:value="300" style="stroke-width: 3px"></line>
                                                    <line x1="42.14285714285714" x2="42.14285714285714" y1="50.1" y2="31.200000000000003" class="ct-bar" ct:value="700" style="stroke-width: 3px"></line>
                                                    <line x1="49.285714285714285" x2="49.285714285714285" y1="39.3" y2="33.9" class="ct-bar" ct:value="200" style="stroke-width: 3px"></line>
                                                    <line x1="56.42857142857143" x2="56.42857142857143" y1="60.9" y2="31.199999999999996" class="ct-bar" ct:value="1100" style="stroke-width: 3px"></line>
                                                </g>
                                            </g>
                                            <g class="ct-labels"></g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="right-chart-content">
                                    <h4>101</h4><span>Purchase ret</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 xl-50 chart_data_right box-col-12">
        <div class="card">
            <div class="card-body">
                <div class="media align-items-center">
                    <div class="media-body right-chart-content">
                        <h4>$95,900<span class="new-box">Hot</span></h4><span>Purchase Order Value</span>
                    </div>
                    <div class="knob-block text-center">
                        <div style="display:inline;width:65px;height:65px;"><canvas width="65" height="65"></canvas><input class="knob1" data-width="10" data-height="70" data-thickness=".3" data-angleoffset="0" data-linecap="round" data-fgcolor="#7366ff" data-bgcolor="#eef5fb" value="60" style="width: 36px; height: 21px; position: absolute; vertical-align: middle; margin-top: 21px; margin-left: -50px; border: 0px; background: none; font: bold 13px Arial; text-align: center; color: rgb(115, 102, 255); padding: 0px; appearance: none;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="col-sm-8">
        <div class="card">
            <div class="card-header">
                <h5>Book Appointment

                    <button class="btn btn-primary active offset-6 " type="button" title="" data-bs-original-title="btn btn-primary active" data-original-title="btn btn-secondary active">Tambah Data</button>
                </h5>
            </div>
            <div class="card-block row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="table-secondary">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Patient Name</th>
                                    <th scope="col">Assigned Doctor</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        <img class="rounded-circle img-30 me-3" src="<?= Url::toRoute('web/images/user/5.jpg'); ?>">
                                    </th>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td><button class="btn btn-warning active  " type="button" title="" data-bs-original-title="btn btn-primary active" data-original-title="btn btn-secondary active">Antrian</button></td>
                                    <td>
                                        <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-pencil"></i> </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><img class="rounded-circle img-30 me-3" src="images/user/5.jpg"></th>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    <td><button class="btn btn-warning active  " type="button" title="" data-bs-original-title="btn btn-primary active" data-original-title="btn btn-secondary active">Antrian</button></td>
                                    <td>
                                        <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-pencil"></i> </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><img class="rounded-circle img-30 me-3" src="images/user/5.jpg"></th>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td><button class="btn btn-danger active  " type="button" title="" data-bs-original-title="btn btn-primary active" data-original-title="btn btn-secondary active">Antrian</button></td>
                                    <td>
                                        <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-pencil"></i> </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><img class="rounded-circle img-30 me-3" src="images/user/5.jpg"></th>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td><button class="btn btn-success active  " type="button" title="" data-bs-original-title="btn btn-primary active" data-original-title="btn btn-secondary active">Antrian</button></td>
                                    <td>
                                        <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-pencil"></i> </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><img class="rounded-circle img-30 me-3" src="images/user/5.jpg"></th>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td><button class="btn btn-danger active  " type="button" title="" data-bs-original-title="btn btn-primary active" data-original-title="btn btn-secondary active">Antrian</button></td>
                                    <td>
                                        <div class="col-sm-6 col-md-4 col-xl-3"><i class="fa fa-pencil"></i> </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-sm-4">
        <div class="card">
            <div class="card-header">
                <h5>Doctor List</h5>
            </div>
            <div class="table-responsive">
                <table class="table table-de">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td><button class="btn btn-outline-danger btn-sm" type="button" title="" data-bs-original-title="btn btn-outline-primary btn-sm" data-original-title="btn btn-outline-primary btn-lg">Absend</button></td>

                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td><button class="btn btn-outline-success btn-sm" type="button" title="" data-bs-original-title="btn btn-outline-primary btn-sm" data-original-title="btn btn-outline-primary btn-lg">Avaiable</button></td>

                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry the Bird</td>
                            <td><button class="btn btn-outline-danger btn-sm" type="button" title="" data-bs-original-title="btn btn-outline-primary btn-sm" data-original-title="btn btn-outline-primary btn-lg">Absend</button></td>

                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry the Bird</td>
                            <td><button class="btn btn-outline-success btn-sm" type="button" title="" data-bs-original-title="btn btn-outline-primary btn-sm" data-original-title="btn btn-outline-primary btn-lg">Avaiable</button></td>

                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry the Bird</td>
                            <td><button class="btn btn-outline-success btn-sm" type="button" title="" data-bs-original-title="btn btn-outline-primary btn-sm" data-original-title="btn btn-outline-primary btn-lg">Avaiable</button></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>

<div class="container-fluid">


    <div class="row size-column">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h5>Multiple tables</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                            <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> entries</label></div>
                            <div id="DataTables_Table_0_filter" class="dataTables_filter offset-1"><label>Search:<input type="search" class="" placeholder="" aria-controls="DataTables_Table_0" data-bs-original-title="" title=""></label></div>
                            <table class="show-case dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                <thead class="table-info">
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 287.875px;">Name</th>
                                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 490.656px;">Position</th>
                                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 189.328px;">Office</th>
                                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 133.266px;">Age</th>
                                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 176.875px;">Salary</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr role="row" class="odd">
                                        <td class="sorting_1">Cedric Kelly</td>
                                        <td>Senior Javascript Developer</td>
                                        <td>Edinburgh</td>
                                        <td>22</td>
                                        <td>$433,060</td>
                                    </tr>
                                    <tr role="row" class="even">
                                        <td class="sorting_1">Dai Rios</td>
                                        <td>Personnel Lead</td>
                                        <td>Edinburgh</td>
                                        <td>35</td>
                                        <td>$217,500</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">Gavin Joyce</td>
                                        <td>Developer</td>
                                        <td>Edinburgh</td>
                                        <td>42</td>
                                        <td>$92,575</td>
                                    </tr>
                                    <tr role="row" class="even">
                                        <td class="sorting_1">Jennifer Acosta</td>
                                        <td>Junior Javascript Developer</td>
                                        <td>Edinburgh</td>
                                        <td>43</td>
                                        <td>$75,650</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">Martena Mccray</td>
                                        <td>Post-Sales support</td>
                                        <td>Edinburgh</td>
                                        <td>46</td>
                                        <td>$324,050</td>
                                    </tr>
                                    <tr role="row" class="even">
                                        <td class="sorting_1">Quinn Flynn</td>
                                        <td>Support Lead</td>
                                        <td>Edinburgh</td>
                                        <td>22</td>
                                        <td>$342,000</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">Shad Decker</td>
                                        <td>Regional Director</td>
                                        <td>Edinburgh</td>
                                        <td>51</td>
                                        <td>$183,000</td>
                                    </tr>
                                    <tr role="row" class="even">
                                        <td class="sorting_1">Sonya Frost</td>
                                        <td>Software Engineer</td>
                                        <td>Edinburgh</td>
                                        <td>23</td>
                                        <td>$103,600</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">Tiger Nixon</td>
                                        <td>System Architect</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td>$320,800</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">Name</th>
                                        <th rowspan="1" colspan="1">Position</th>
                                        <th rowspan="1" colspan="1">Office</th>
                                        <th rowspan="1" colspan="1">Age</th>
                                        <th rowspan="1" colspan="1">Salary</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous" data-bs-original-title="" title="">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" data-bs-original-title="" title="">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next" data-bs-original-title="" title="">Next</a></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 xl-50 box-col-12 ">
            <div class="card">
                <div class="card-header card-no-border">
                    <h5>News &amp; Updates</h5>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li><i class="fa fa-spin fa-cog"></i></li>
                            <li><i class="view-html fa fa-code"></i></li>
                            <li><i class="icofont icofont-maximize full-card"></i></li>
                            <li><i class="icofont icofont-minus minimize-card"></i></li>
                            <li><i class="icofont icofont-refresh reload-card"></i></li>
                            <li><i class="icofont icofont-error close-card"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body new-update pt-0">
                    <div class="activity-timeline">
                        <div class="media">
                            <div class="activity-line"></div>
                            <div class="activity-dot-secondary"></div>
                            <div class="media-body"><span>Update Product</span>
                                <p class="font-roboto">Quisque a consequat ante Sit amet magna at volutapt.</p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="activity-dot-primary"></div>
                            <div class="media-body"><span>James liked Nike Shoes</span>
                                <p class="font-roboto">Aenean sit amet magna vel magna fringilla ferme.</p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="activity-dot-secondary"></div>
                            <div class="media-body"><span>john just buy your product<i class="fa fa-circle circle-dot-secondary pull-right"></i></span>
                                <p class="font-roboto">Vestibulum nec mi suscipit, dapibus purus.....</p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="activity-dot-primary"></div>
                            <div class="media-body"><span>Jihan Doe just save your product<i class="fa fa-circle circle-dot-primary pull-right"></i></span>
                                <p class="font-roboto">Curabitur egestas consequat lorem.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
</div>
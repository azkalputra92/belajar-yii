<?php

namespace backend\controllers;

use Yii;
use common\models\TaPemesanan;
use backend\models\TaPemesananSearch;
use backend\models\RefSupplierSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use common\models\TaPemesananDetail;
use yii\helpers\ArrayHelper;

/**
 * PemesananController implements the CRUD actions for TaPemesanan model.
 */
class PemesananController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaPemesanan models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $request = Yii::$app->request;
        $searchModel = new TaPemesananSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status'=>1]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Pemesanan  ",
                    'content'=>$this->renderAjax('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexProses()
    {    
        $request = Yii::$app->request;
        $searchModel = new TaPemesananSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status'=>2]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Pemesanan Proses ",
                    'content'=>$this->renderAjax('index_proses', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index_proses', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexSampai()
    {    
        $request = Yii::$app->request;
        $searchModel = new TaPemesananSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status'=>3]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Pemesanan Sampai  ",
                    'content'=>$this->renderAjax('index_sampai', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index_sampai', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexSelesai()
    {    
        $request = Yii::$app->request;
        $searchModel = new TaPemesananSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_status'=>100]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Pemesanan Selesai  ",
                    'content'=>$this->renderAjax('index_selesai', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index_selesai', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single TaPemesanan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> Yii::t('kvgrid','View')." Pemesanan  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                            // Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TaPemesanan model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TaPemesanan();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Create New')." pemesanan obat ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> Yii::t('kvgrid','Create New')." pemesanan obat ",
                    'content'=>'<span class="text-success">'.Yii::t('kvgrid','Create').' TaPemesanan '.Yii::t('kvgrid','success').'</span>',
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Create More'),['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> Yii::t('kvgrid','Create New')." Pemesanan ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    public function actionPesan(){
        $request = Yii::$app->request;
        $searchModel = new RefSupplierSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Pilih Supplier ",
                    'content'=>$this->renderAjax('index_pilih_supplier', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index_pilih_supplier', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionPilih($id_supplier)
    {
        $request = Yii::$app->request;
        $model = new TaPemesanan();  
        $model->id_supplier = $id_supplier;
        $model->tanggal_pesan = date('Y-m-d');
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        
        try {
            if($model->save()){
                $model->setStatus(1);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pemesanan-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['pemesanan/index']);
        }
    }


    public function actionProsesPesan($id_pemesanan)
    {
        $request = Yii::$app->request;
        $model = TaPemesanan::find()->where(['id'=>$id_pemesanan])->one();
        $model->setStatus(2);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pemesanan-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionProsesSampai($id_pemesanan)
    {
        $request = Yii::$app->request;
        $model = TaPemesanan::find()->where(['id'=>$id_pemesanan])->one();
        $model->setStatus(3);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pemesanan-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionProsesSelesai($id_pemesanan)
    {
        $request = Yii::$app->request;
        $model = TaPemesanan::find()->where(['id'=>$id_pemesanan])->one();
        $model->setStatus(100);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pemesanan-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // public function actionPilih($id_supplier)
    // {
    //     $request = Yii::$app->request;
    //     $model = new TaPemesanan();  
    //     $model->id_supplier = $id_supplier;
    //     $model->tanggal_pesan = date('Y-m-d');
        
        

    //     if($request->isAjax){
    //         /*
    //         *   Process for ajax request
    //         */
    //         Yii::$app->response->format = Response::FORMAT_JSON;
            
    //          if($model->load($request->post())){

    //             $connection = \Yii::$app->db;
    //             $transaction = $connection->beginTransaction();
    //             try {
    //                 if($model->save()){
    //                     $model->setStatus(1);
    //                 }
    //                 $transaction->commit();
    //             } catch (\Exception $e) {
    //                 $transaction->rollBack();
    //                 throw $e;
    //             } catch (\Throwable $e) {
    //                 $transaction->rollBack();
    //                 throw $e;
    //             }

    //             return [
    //                 'forceReload'=>'#crud-datatable-pemesanan-pjax',
    //                 'title'=> Yii::t('kvgrid','Create New')." pemesanan obat ",
    //                 'content'=>'<span class="text-success">'.Yii::t('kvgrid','Create').' TaPemesanan '.Yii::t('kvgrid','success').'</span>',
    //                 'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
    //                         Html::a(Yii::t('kvgrid','Create More'),['pesan'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
    //             ];         
    //         }
    //     }else{
    //         /*
    //         *   Process for non-ajax request
    //         */
    //         if ($model->load($request->post()) ) {
    //             $connection = \Yii::$app->db;
    //             $transaction = $connection->beginTransaction();
    //             try {
    //                 if($model->save()){
    //                     $model->setStatus(1);
    //                 }
    //                 $transaction->commit();
    //             } catch (\Exception $e) {
    //                 $transaction->rollBack();
    //                 throw $e;
    //             } catch (\Throwable $e) {
    //                 $transaction->rollBack();
    //                 throw $e;
    //             }
    //             return $this->redirect(['view', 'id' => $model->id]);
    //         }
    //     }
    // }

    /**
     * Updates an existing TaPemesanan model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Update')." Pemesanan  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pemesanan-pjax',
                    'title'=> Yii::t('kvgrid','View')." Pemesanan  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> Yii::t('kvgrid','View')." Pemesanan  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing TaPemesanan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pemesanan-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing TaPemesanan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the TaPemesanan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaPemesanan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaPemesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

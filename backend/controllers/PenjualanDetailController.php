<?php

namespace backend\controllers;

use backend\models\RefObatSearch;
use backend\models\TaPenjualan;
use Yii;
use common\models\TaPenjualanDetail;
use backend\models\TaPenjualanDetailSearch as TaPenjualanDetailSearch;
use common\models\RefObat;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * PenjualanDetailController implements the CRUD actions for TaPenjualanDetail model.
 */
class PenjualanDetailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaPenjualanDetail models.
     * @return mixed
     */
    public function actionIndex($id_penjualan)
    {    
        $request = Yii::$app->request;
        $searchModel = new TaPenjualanDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_penjualan' => $id_penjualan]);
        
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Penjualan Detail  ",
                    'content'=>$this->renderAjax('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'id_penjualan' => $id_penjualan,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_penjualan' => $id_penjualan,
            ]);
        }
    }

    public function actionIndexDetail($id_penjualan)
    {    
        $request = Yii::$app->request;
        $searchModel = new TaPenjualanDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_penjualan' => $id_penjualan]);
        
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Penjualan Detail  ",
                    'content'=>$this->renderAjax('index-detail', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'id_penjualan' => $id_penjualan,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index-detail', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_penjualan' => $id_penjualan,
            ]);
        }
    }


    /**
     * Displays a single TaPenjualanDetail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> Yii::t('kvgrid','View')." Penjualan Detail  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TaPenjualanDetail model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $searchModel = new RefObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Obat  ",
                    'content'=>$this->renderAjax('index-obat', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index-obat', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexObat($id_penjualan)
    {
        $request = Yii::$app->request;
        $searchModel = new RefObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['not', ['stok' => null]]);
        $dataProvider->query->andWhere(['not', ['stok' => 0,]]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Obat  ",
                    'content'=>$this->renderAjax('index-obat', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'id_penjualan' => $id_penjualan,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index-obat', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_penjualan' => $id_penjualan,
            ]);
        }
    }

    public function actionPilihObat($id_obat, $id_penjualan, $harga)
    {
        $request = Yii::$app->request;
        $model = new TaPenjualanDetail();
        $modelObat = RefObat::findOne(['id' => $id_obat]);

        $model->id_obat = $id_obat;
        $model->id_penjualan = $id_penjualan;
        $model->harga = $harga;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Tentukan '). "Jumlah Obat",
                    'content'=>$this->renderAjax('pilih-obat', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Batal'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Add'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate()){
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {

                    if(($model->jumlah <= 0) || ($model->jumlah > $modelObat->stok) ) throw new \yii\web\HttpException(400, '');  
                    $modelObat->stok -= $model->jumlah;
                    $modelObat->save();

                    $model->setHarga();
                    $model->setTotalPenjualan();

                    $transaction->commit();  
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return [
                        'forceReload'=>'#crud-datatable-penjualan-pjax',
                        'title'=> Yii::t('kvgrid','Error !'),
                        'content'=>'<span class="text-danger">'.Yii::t('kvgrid','Add').' Jumlah '.Yii::t('kvgrid','Gagal ! jumlah harus lebih kecil dari stok dan bukan 0').'</span>',
                        'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
            
                    ];         
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
        
                return [
                    'forceReload'=>'#crud-datatable-penjualan-pjax',
                    'title'=> Yii::t('kvgrid','Tentukan '). "Jumlah Obat",
                    'content'=>'<span class="text-success">'.Yii::t('kvgrid','Create').' TaPenjualanDetail '.Yii::t('kvgrid','success').'</span>',
                    'footer'=> Html::button(Yii::t('kvgrid','Batal'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Create More'),['penjualan-detail/index-obat', 'id_penjualan' => $id_penjualan],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> Yii::t('kvgrid','Tentukan '). "Jumlah Obat",
                    'content'=>$this->renderAjax('pilih-obat', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Batal'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]). Html::button(Yii::t('kvgrid','Add'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {

                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {

                    if(($model->jumlah <= 0) || ($model->jumlah > $modelObat->stok) ) throw new \yii\web\HttpException(400, '');  
                    $modelObat->stok -= $model->jumlah;
                    $modelObat->save();

                    $model->setHarga();
                    $model->setTotalPenjualan();

                    $transaction->commit();  
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return [
                        'forceReload'=>'#crud-datatable-penjualan-pjax',
                        'title'=> Yii::t('kvgrid','Error !'),
                        'content'=>'<span class="text-danger">'.Yii::t('kvgrid','Add').' Jumlah '.Yii::t('kvgrid','Gagal ! jumlah harus lebih kecil dari stok dan bukan 0').'</span>',
                        'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
            
                    ];         
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
        
                
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('pilih-obat', [
                    'model' => $model,
                ]);
            }
        }
    }

  

    /**
     * Updates an existing TaPenjualanDetail model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Update')." Penjualan Detail  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->validate()){
                $model->setHarga();
                $model->setTotalPenjualan();

                return [
                    'forceReload'=>'#crud-datatable-penjualan-pjax',
                    'title'=> Yii::t('kvgrid','View')." Penjualan Detail  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> Yii::t('kvgrid','View')." Penjualan Detail  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {
                $model->setHarga();
                $model->setTotalPenjualan();

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing TaPenjualanDetail model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $thisModel = $this->findModel($id);
        $obatModel = RefObat::findOne(['id' => $thisModel->id_obat]);
        // $penjualanModel = TaPenjualan::findOne(['id' => $thisModel->id_penjualan]);
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $obatModel->stok += $thisModel->jumlah;
            $obatModel->save();
            // $penjualanModel->save();
            $transaction->commit();
            $thisModel->delete();
            $thisModel->setTotalPenjualan();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
  
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-penjualan-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['penjualan/index']);
        }


    }

     /**
     * Delete multiple existing TaPenjualanDetail model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the TaPenjualanDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaPenjualanDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaPenjualanDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\TaPemesananDetail;


return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // 'vAlign' => 'middle',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'supplier.supplier',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tanggal_pesan',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format'=>'currency',
        'attribute'=>'total_harga',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'statusPemesanan.status',
        'vAlign' => 'middle',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
        // 'vAlign' => 'middle',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
        // 'vAlign' => 'middle',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{view} {delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
                          'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'template' => '{detail},{pesan}',
        'buttons' => [
            "detail" => function ($url, $model, $key) {
                return  Html::a('Detail', ['pemesanan-detail/index', 'id_pemesanan' => $model->id, 'id_supplier' => $model->id_supplier], [
                    'class' => 'btn btn-primary mb-2',
                    'title'=>Yii::t('kvgrid','Daftar/Pilih Obat'), 
                    //'data-pjax' => '0',
                    'role' => 'modal-remote',
                    'target' => 'modal'
                ]);
            },
            "pesan" => function ($url, $model, $key) {
                $jumlahObat = TaPemesananDetail::find()->where(['id_pemesanan' => $model->id])->count() ;
                $status = ($jumlahObat < 1) ? 'disabled' : '';
                
                return  Html::a('Proses Pesanan', ['proses-pesan', 'id_pemesanan' => $model->id], [
                    'class' => "btn btn-success mb-2 {$status}",
                    'role'=>'modal-remote','title'=>Yii::t('kvgrid','Pesan Obat'), 
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
                    'data-confirm-message'=>Yii::t('kvgrid','Apakah anda ingin melakukan proses pemesanan?') 
                ]);
            }
        ]
    ],

];   
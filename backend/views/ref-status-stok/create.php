<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RefStatusStok */

$this->title = Yii::t('kvgrid','Add').' Ref Status Stok ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ref-status-stok-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

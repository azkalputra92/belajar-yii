<?php

namespace backend\controllers;

use Yii;
use common\models\RefSupplierObat;
use backend\models\RefSupplierObatSearch;
use backend\models\RefObatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * SupplierObatController implements the CRUD actions for RefSupplierObat model.
 */
class SupplierObatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefSupplierObat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new RefSupplierObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Supplier Obat  ",
                'content' => $this->renderAjax('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionTambah($id_supplier)
    {
        $request = Yii::$app->request;
        $searchModel = new RefObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Supplier Obat  ",
                'content' => $this->renderAjax('index_tambah', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_supplier' => $id_supplier,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_tambah', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_supplier' => $id_supplier,
            ]);
        }
    }

    public function actionPilih($id_obat, $id_supplier)
    {
        $request = Yii::$app->request;
        // $this->findModel($id)->delete();
        $model = new RefSupplierObat();
        $model->id_obat = $id_obat;
        $model->id_supplier = $id_supplier;
        $model->save();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload' => '#crud-datatable-supplier-pjax',
                'title' => Yii::t('kvgrid', 'Create New') . " Supplier Obat ",
                'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' RefSupplierObat ' . Yii::t('kvgrid', 'success') . '</span>',
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('kvgrid', 'Create More'), ['tambah', 'id_supplier' => $id_supplier], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

            ];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // public function actionPilih($id_obat, $id_supplier)
    // {
    //     $request = Yii::$app->request;
    //     $model = new RefSupplierObat();
    //     $model->id_supplier = $id_supplier;
    //     $model->id_obat = $id_obat;


    //     if ($request->isAjax) {
    //         /*
    //         *   Process for ajax request
    //         */
    //         Yii::$app->response->format = Response::FORMAT_JSON;
    //         if ($request->isGet) {
    //             return [
    //                 'title' => Yii::t('kvgrid', 'Create New') . " Konsultasi ",
    //                 'content' => $this->renderAjax('create', [
    //                     'model' => $model,
    //                 ]),
    //                 'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
    //                     Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

    //             ];
    //         } else if ($model->load($request->post()) && $model->save()) {
    //             return [
    //                 'forceReload' => '#crud-datatable-supplier-obat-pasiens-pjax',
    //                 'title' => Yii::t('kvgrid', 'Create New') . " Konsultasi ",
    //                 'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' TaKonsultasi ' . Yii::t('kvgrid', 'success') . '</span>',
    //                 'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
    //                     Html::a(Yii::t('kvgrid', 'Create More'), ['pilih', 'id_obat' => $id_obat, 'id_supplier' => $id_supplier], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

    //             ];
    //         } else {
    //             return [
    //                 'title' => Yii::t('kvgrid', 'Create New') . " Konsultasi ",
    //                 'content' => $this->renderAjax('create', [
    //                     'model' => $model,
    //                 ]),
    //                 'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
    //                     Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

    //             ];
    //         }
    //     } else {
    //         /*
    //         *   Process for non-ajax request
    //         */
    //         if ($model->load($request->post()) && $model->save()) {
    //             return $this->redirect(['view', 'id' => $model->id]);
    //         } else {
    //             return $this->render('create', [
    //                 'model' => $model,
    //             ]);
    //         }
    //     }
    // }

    /**
     * Displays a single RefSupplierObat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = null)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => Yii::t('kvgrid', 'View') . " Supplier Obat  ",
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('kvgrid', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionObat($id_supplier)
    {
        $request = Yii::$app->request;
        $searchModel = new RefSupplierObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_supplier' => $id_supplier]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Stok  ",
                'content' => $this->renderAjax('index_obat', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_supplier' => $id_supplier,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_obat', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_supplier' => $id_supplier,
            ]);
        }
    }

    /**
     * Creates a new RefSupplierObat model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new RefSupplierObat();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Supplier Obat ",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-supplier-obat-pjax',
                    'title' => Yii::t('kvgrid', 'Create New') . " Supplier Obat ",
                    'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' RefSupplierObat ' . Yii::t('kvgrid', 'success') . '</span>',
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::a(Yii::t('kvgrid', 'Create More'), ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Supplier Obat ",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing RefSupplierObat model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Update') . " Supplier Obat  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-supplier-obat-pjax',
                    'title' => Yii::t('kvgrid', 'View') . " Supplier Obat  ",
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::a(Yii::t('kvgrid', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'View') . " Supplier Obat  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing RefSupplierObat model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-supplier-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing RefSupplierObat model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-supplier-obat-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the RefSupplierObat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefSupplierObat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefSupplierObat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\FontAsset;
use common\widgets\Alert;
use frontend\assets\AppAsset;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

FontAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="landing-page">
<?php $this->beginBody() ?>
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper landing-page">
      <!-- Page Body Start            -->
      <div class="landing-home">
        <ul class="decoration">
          <li class="one"><img class="img-fluid" src="/images/landing/decore/1.png" alt=""></li>
          <li class="two"><img class="img-fluid" src="/images/landing/decore/2.png" alt=""></li>
          <li class="three"><img class="img-fluid" src="/images/landing/decore/4.png" alt=""></li>
          <li class="four"><img class="img-fluid" src="/images/landing/decore/3.png" alt=""></li>
          <li class="five"><img class="img-fluid" src="/images/landing/2.png" alt=""></li>
          <li class="six"><img class="img-fluid" src="/images/landing/decore/cloud.png" alt=""></li>
          <li class="seven"><img class="img-fluid" src="/images/landing/2.png" alt=""></li>
        </ul>
        <div class="container-fluid">
          <div class="sticky-header">
            <header>                       
              <nav class="navbar navbar-b navbar-trans navbar-expand-xl fixed-top nav-padding" id="sidebar-menu"><a class="navbar-brand p-0" href="#"><img class="img-fluid" src="/images/landing/landing_logo.png" alt=""></a>
                <button class="navbar-toggler navabr_btn-set custom_nav" type="button" data-bs-toggle="collapse" data-bs-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation"><span></span><span></span><span></span></button>
                <div class="navbar-collapse justify-content-end collapse hidenav" id="navbarDefault">
                  <ul class="navbar-nav navbar_nav_modify" id="scroll-spy">
                    <li class="nav-item"><a class="nav-link" href="#akses">Akses</a></li>
                    <li class="nav-item"><a class="nav-link" href="#dokter">Jadwal</a></li>
                    <!-- <li class="nav-item"><a class="nav-link" href="#layout">Layouts</a></li> -->
                    <!-- <li class="nav-item"><a class="nav-link" href="#frameworks">Frameworks</a></li> -->
                    <!-- <li class="nav-item"><a class="nav-link" href="#components">Components</a></li> -->
                    <!-- <li class="nav-item"><a class="nav-link" href="#applications">Applications</a></li> -->
                    <li class="nav-item buy-btn"><a class="nav-link js-scroll" href="#" target="_blank">Antrian</a></li>
                  </ul>
                </div>
              </nav>
            </header>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-6">
              <div class="content">
                <div>
                  <h1 class="wow fadeIn">SELAMAT DATANG</h1>
                  <h1 class="wow fadeIn">Di Aplikasi Klinik</h1>
                  <h2 class="txt-secondary wow fadeIn">Admin, Dokter dan Terapis</h2>
                  <p class="mt-3 wow fadeIn">Silahkan login ke aplikasi berdasarkan jenis akun anda.</p>
                  <div class="btn-grp mt-4">
                    <a class="btn btn-pill btn-primary btn-air-primary btn-lg me-3 wow pulse" href="admin"> <img src="/images/landing/icon/html/html.png" alt="">Admin</a>
                    <a class="btn btn-pill btn-secondary btn-air-secondary btn-lg me-3 wow pulse" href="dokter"><img src="/images/landing/icon/react/react.png" alt="">Dokter</a>
                    <a class="btn btn-pill btn-info btn-air-info btn-lg wow pulse" href="perawat"> <img src="/images/landing/icon/angular/angular.svg" alt="">Terapis</a></div>
                  <!-- <div class="btn-grp mt-4"><a class="btn btn-pill btn-secondary btn-air-secondary btn-lg wow pulse me-3" href="https://vue.pixelstrap.com/cuba/dashboard/default" target="_blank"> <img src="/images/landing/icon/vue/vue.png" alt="">VueJs</a><a class="btn btn-pill btn-success btn-air-success btn-lg wow pulse me-3" href="http://laravel.pixelstrap.com/cuba/pages/landing" target="_blank"> <img src="/images/landing/icon/laravel/laravel2.png" alt="">Laravel</a></div> -->
                </div>
              </div>
            </div>
            <div class="col-xl-7 col-lg-6">                 
              <div class="wow fadeIn"><img class="screen1" src="/images/landing/screen1.jpg" alt=""></div>
              <div class="wow fadeIn"><img class="screen2" src="/images/landing/screen3.jpg" alt=""></div>
            </div>
          </div>
        </div>
      </div>
      <section class="section-space cuba-demo-section layout" id="akses">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 wow pulse">
              <div class="cuba-demo-content">
                <div class="couting">
                  <h2>Login</h2>
                  <p>Pilih Jenis Akses Anda</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row demo-imgs">
            <div class="col-lg-4 col-md-6 wow pulse demo-content">
              <div class="cuba-demo-img">
                <div class="overflow-hidden"><img class="img-fluid" src="/images/landing/layout-images/dubai.jpg" alt="default"></div>
                <div class="hover-link">
                  <div class="link-btn" data-bs-toggle="tooltip" data-bs-placement="top" title="Login">
                    <a class="ms-2" href="/admin">Login</a>
                  </div>
                </div>
              </div>
              <div class="title-wrapper">
                <div class="content">
                  <h3 class="theme-name mb-0">Admin</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow pulse demo-content">
              <div class="cuba-demo-img">
                <div class="overflow-hidden"><img class="img-fluid" src="/images/landing/layout-images/los.jpg" alt="material"></div>
                <div class="hover-link">
                  <div class="link-btn" data-bs-toggle="tooltip" data-bs-placement="top" title="Login">
                    <a class="ms-2" href="/dokter">Login</a>
                  </div>
                </div>
              </div>
              <div class="title-wrapper">
                <div class="content">
                  <h3 class="theme-name mb-0">Dokter</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow pulse demo-content">
              <div class="cuba-demo-img">
                <div class="overflow-hidden"><img class="img-fluid" src="/images/landing/layout-images/london.jpg" alt="blank"></div>
                <div class="hover-link">
                  <div class="link-btn" data-bs-toggle="tooltip" data-bs-placement="top" title="Login">
                    <a class="ms-2" href="/perawat">Login</a>
                  </div>
                </div>
              </div>
              <div class="title-wrapper">
                <div class="content">
                  <h3 class="theme-name mb-0">Terapis</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- <section class="section-space cuba-demo-section app_bg frameworks-section" id="dokter">
        <div class="container">
          <div class="row">                 
            <div class="col-sm-12 wow pulse">
              <div class="cuba-demo-content mt50">
                <div class="couting">
                  <h2>Jadwal</h2>
                </div>
                <p class="mb-0">Dokter Hari Ini</p>
              </div>
            </div>
            <div class="col-sm-12 framworks">                 
              <ul class="nav nav-pills justify-content-center" id="pills-tab" role="tablist">
                <li class="nav-item"><a class="nav-link d-flex active" id="pills-home-tab" data-bs-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"> <img src="/images/landing/icon/html/html.png" alt="">
                    <div class="text-start">
                      <h5 class="mb-0">HTML</h5>
                      <p class="mb-0">Frameworks</p>
                    </div></a></li>
                <li class="nav-item"><a class="d-flex nav-link" id="pills-profile-tab" data-bs-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"> <img src="/images/landing/icon/react/react1.png" alt="">
                    <div class="text-start">
                      <h5 class="mb-0">React</h5>
                      <p class="mb-0">Frameworks</p>
                    </div></a></li>
                <li class="nav-item"><a class="d-flex nav-link" id="pills-angular-tab" data-bs-toggle="pill" href="#pills-angular" role="tab" aria-controls="pills-angular" aria-selected="false"> <img src="/images/landing/icon/angular/angular.svg" alt="">
                    <div class="text-start">
                      <h5 class="mb-0">Angular</h5>
                      <p class="mb-0">Frameworks</p>
                    </div></a></li>
                <li class="nav-item"><a class="d-flex nav-link" id="pills-vue-tab" data-bs-toggle="pill" href="#pills-vue" role="tab" aria-controls="pills-vue" aria-selected="false"> <img src="/images/landing/icon/vue/vue.png" alt="">
                    <div class="text-start">
                      <h5 class="mb-0">Vue</h5>
                      <p class="mb-0">Frameworks</p>
                    </div></a></li>
                <li class="nav-item"><a class="d-flex nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"> <img src="/images/landing/icon/laravel/laravel.png" alt="">
                    <div class="text-start">
                      <h5 class="mb-0">Laravel</h5>
                      <p class="mb-0">Frameworks</p>
                    </div></a></li>
              </ul>
              <div class="tab-content mt-5" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                  <ul class="framworks-list">
                    <li class="box wow fadeInUp">
                      <div> <img src="/images/landing/icon/html/bootstrap.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Booxstrap 4X</h6>
                    </li>
                    <li class="box wow fadeInUp">
                      <div> <img src="/images/landing/icon/html/css.png" alt=""></div>
                      <h6 class="mb-0 mt-3">CSS</h6>
                    </li>
                    <li class="box wow fadeInUp">
                      <div> <img src="/images/landing/icon/html/sass.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Sass</h6>
                    </li>
                    <li class="box wow fadeInUp">
                      <div> <img src="/images/landing/icon/html/pug.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Pug</h6>
                    </li>
                    <li class="box wow fadeInUp">
                      <div> <img src="/images/landing/icon/html/npm.png" alt=""></div>
                      <h6 class="mb-0 mt-3">NPM</h6>
                    </li>
                    <li class="box wow fadeInUp">
                      <div> <img src="/images/landing/icon/html/gulp.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Gulp</h6>
                    </li>
                    <li class="box wow bounceIn">                                   
                      <div> <img src="/images/landing/icon/html/kit.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Starter Kit</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/html/uikits.png" alt=""></div>
                      <h6 class="mb-0 mt-3">40+ UI Kits</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/html/layout.png" alt=""></div>
                      <h6 class="mb-0 mt-3">8+ Layout</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/html/builders.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Builders</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/html/iconset.png" alt=""></div>
                      <h6 class="mb-0 mt-3">11 Icon Sets</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/html/forms.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Forms</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/html/table.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Tables</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/html/apps.png" alt=""></div>
                      <h6 class="mb-0 mt-3">17+ Apps</h6>
                    </li>
                  </ul>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                  <ul class="framworks-list">
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/hook.png" alt=""></div>
                      <h6 class="mb-0 mt-3">React Hook</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/reactstrap.png" alt=""></div>
                      <h6 class="mb-0 mt-3">React Strap</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/noquery.png" alt=""></div>
                      <h6 class="mb-0 mt-3">No Jquery</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/redux.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Redux</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/firebase.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Firebase Auth</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/crud.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Firebase Crud</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/chat.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Chat</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/animation.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Router Animation</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/props_state.png" alt=""></div>
                      <h6 class="mb-0 mt-3">State & Props</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/react/reactrouter.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Reactrouter</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/react/chart.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Amazing Chart</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/react/map.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Map</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/react/gallery.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Gallery</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/react/application.png" alt=""></div>
                      <h6 class="mb-0 mt-3">9+ Apps</h6>
                    </li>
                  </ul>
                </div>
                <div class="tab-pane fade" id="pills-angular" role="tabpanel" aria-labelledby="pills-angular-tab">
                  <ul class="framworks-list">
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/1.png" alt=""></div>
                      <h6 class="mb-0 mt-3">SCSS</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/2.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Ng Bootstrap</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/3.png" alt=""></div>
                      <h6 class="mb-0 mt-3">RXjs</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/4.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Router</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/5.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Form</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/6.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Apex chart</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/7.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Chart.js</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/8.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Chartist</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/9.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Google Map</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/angular/10.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Gallery</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/angular/11.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Ecommerce</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/angular/12.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Firebase Auth</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/angular/13.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Firebase Crud</h6>
                    </li>
                    <li class="box wow bounceIn">                                    
                      <div> <img src="/images/landing/icon/angular/14.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Chat</h6>
                    </li>
                  </ul>
                </div>
                <div class="tab-pane fade" id="pills-vue" role="tabpanel" aria-labelledby="pills-vue-tab">
                  <ul class="framworks-list">
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/firebase.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Firebase</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/nojquery.png" alt=""></div>
                      <h6 class="mb-0 mt-3">No jquery</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/vuebootstrap.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Vue Bootstrap</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/vuex.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Vuex</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/chart.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Chart</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/vueswiper.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Vue Swiper</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/vuerouter.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Vue Router</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/vuemasonary.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Vue Masonary</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/vuecli.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Vue Cli</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/animation.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Animation</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/rangeslider.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Range Slider</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/vue/rtlsupport.png" alt=""></div>
                      <h6 class="mb-0 mt-3">RTL Support</h6>
                    </li>
                  </ul>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                  <ul class="framworks-list">
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/laravel.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Laravel 7</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/bootstrap.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Bootstrap 5</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/html/sass.png" alt=""></div>
                      <h6 class="mb-0 mt-3">SASS</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/blade.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Blade</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/layouts.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Layouts</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/npm.png" alt=""></div>
                      <h6 class="mb-0 mt-3">NPM</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/mix.png" alt=""></div>
                      <h6 class="mb-0 mt-3">MIX</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/yarn.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Yarn</h6>
                    </li>
                    <li class="box">
                      <div> <img src="/images/landing/icon/laravel/sasswebpack.png" alt=""></div>
                      <h6 class="mb-0 mt-3">Sasswebpack</h6>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> -->
      
      <!-- <footer class="footer-bg">
        <div class="container">
          <div class="landing-center ptb50">
            <div class="title"><img class="img-fluid" src="/images/landing/landing_logo.png" alt=""></div>
            <div class="footer-content">
              <h1>The Cuba Bootstrap Admin Theme Trusted By Many Developers World Wide.</h1>
              <p>If You like Our Theme So Please Rate Us.</p><a class="btn mrl5 btn-lg btn-primary default-view" target="_blank" href="index.html">Check Now</a><a class="btn mrl5 btn-lg btn-secondary btn-md-res" target="_blank" href="https://1.envato.market/3GVzd">Buy Now                    </a>
            </div>
          </div>
        </div>
      </footer> -->
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();

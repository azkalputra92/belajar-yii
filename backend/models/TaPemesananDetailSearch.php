<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaPemesananDetail;

/**
 * TaPemesananDetailSearch represents the model behind the search form about `common\models\TaPemesananDetail`.
 */
class TaPemesananDetailSearch extends TaPemesananDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pemesanan', 'id_obat', 'harga', 'jumlah', 'total'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaPemesananDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_pemesanan' => $this->id_pemesanan,
            'id_obat' => $this->id_obat,
            'harga' => $this->harga,
            'jumlah' => $this->jumlah,
            'total' => $this->total,
        ]);

        return $dataProvider;
    }
}

<?php
use yii\helpers\Url;
use yii\helpers\Html;


return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // 'vAlign' => 'middle',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'id_pemesanan',
    //     'vAlign' => 'middle',
    // ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'obat.obat',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format'=>'Currency',
        'attribute'=>'harga',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'jumlah',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Jumlah Rusak',
        'vAlign' => 'middle',
        'value' => function($model){
            return ($model->taPemesananDetailReturn)?$model->taPemesananDetailReturn->jumlah : '';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format'=>'Currency',
        'attribute'=>'total',
        'vAlign' => 'middle',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'template' => '{rusak}',
        'buttons' => [
            "rusak" => function ($url, $model, $key) {
                $tombol = Html::a('Obat Tidak Sesuai', ['obat-rusak', 'id' => $model->id], [
                    'class' => "btn btn-primary mb-2",
                    'title' => Yii::t('kvgrid', 'Obat Tidak Sesuai'),
                    'role' => 'modal-remote',
                    'target' => 'modal'
                ]);
                if($model->taPemesananDetailReturn){
                    if($model->jumlah > $model->taPemesananDetailReturn->jumlah){
                        return $tombol;
                    }
                }else{
                    return $tombol;
                }
            },
        ]
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'id_status',
    //     'vAlign' => 'middle',
    // ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'template'=>'',
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'id'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
    //                       'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    // ]
];   
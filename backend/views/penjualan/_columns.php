<?php

use backend\models\TaPenjualanSearch;
use backend\models\TaPenjualanDetailSearch;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // 'vAlign' => 'middle',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'no_invoice',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tanggal',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format' => 'currency',
        'attribute'=>'total_harga',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'statusPenjualan.status',
        'vAlign' => 'middle',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
        // 'vAlign' => 'middle',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
        // 'vAlign' => 'middle',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'template' => '{detail}{proses}',
        'buttons' => [
            "detail" => function ($url, $model, $key) {
                return  Html::a('Detail', ['penjualan-detail/index', 'id_penjualan' => $model->id], [
                    'class' => 'btn btn-primary mb-2',
                    'title'=>Yii::t('kvgrid','Daftar/Pilih Obat'), 
                    //'data-pjax' => '0',
                    'role' => 'modal-remote',
                    'target' => 'modal'
                ]);
            },
            "proses" => function ($url, $model, $key) {
                $jumlahPesanan = TaPenjualanDetailSearch::find()->where(['id_penjualan' => $model->id])->count() ;
                $status = ($jumlahPesanan < 1) ? 'disabled' : '';
                return  Html::a('Siapkan', ['penjualan/siapkan', 'id_penjualan' => $model->id], [
                        'class' => "btn btn-success mb-2 {$status}",
                        'title'=>Yii::t('kvgrid','Siapkan'), 
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
                        'data-confirm-message'=>Yii::t('kvgrid','Apakah anda yakin akan menyiapkan pesanan ini ?')]);
            },
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
                          'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    ],
  

];   
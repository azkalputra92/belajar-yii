<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaPemesananDetailReturn;

/**
 * TaPemesananDetailReturnSearch represents the model behind the search form about `common\models\TaPemesananDetailReturn`.
 */
class TaPemesananDetailReturnSearch extends TaPemesananDetailReturn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pemesanan', 'id_obat', 'jumlah', 'id_return'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaPemesananDetailReturn::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_pemesanan' => $this->id_pemesanan,
            'id_obat' => $this->id_obat,
            'jumlah' => $this->jumlah,
            'id_return' => $this->id_return,
        ]);

        return $dataProvider;
    }
}

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaPenjualanDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!Yii::$app->request->isAjax){ ?>
<div class="row">
<div class="col-sm-12">
<div class="card">
<div class="card-body">
<?php } ?>

    <?php $form = ActiveForm::begin(); ?>

    <?php //$form->field($model, 'id_penjualan')->textInput() ?>

    <?php //$form->field($model, 'id_obat')->textInput() ?>

    <?php //$form->field($model, 'harga')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?php //$form->field($model, 'total')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Menambahkan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    

<?php if (!Yii::$app->request->isAjax){ ?>
</div>
</div>
</div>
</div>
<?php } ?>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RefStatusPemesanan */

$this->title = Yii::t('kvgrid','Update').' Status Pemesanan ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ref-status-pemesanan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

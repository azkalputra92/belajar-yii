<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaPemesananStatus */

$this->title = Yii::t('kvgrid','Add').' Pemesanan Status ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-pemesanan-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

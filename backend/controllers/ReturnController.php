<?php

namespace backend\controllers;

use Yii;
use common\models\TaReturn;
use common\models\RefObat;
use common\models\TaPemesananDetailReturn;
use backend\models\TaReturnSearch;
use backend\models\TaPemesananDetailReturnSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ReturnController implements the CRUD actions for TaReturn model.
 */
class ReturnController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaReturn models.
     * @return mixed
     */
    public function actionIndex($status = null)
    {    
        $request = Yii::$app->request;
        $searchModel = new TaReturnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($status){
            $dataProvider->query->andWhere(['id_status'=>$status]);
        }else{
            $dataProvider->query->andWhere(['id_status'=>1]);
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Return  ",
                    'content'=>$this->renderAjax('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'status' => $status,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'status' => $status,
            ]);
        }
    }
    public function actionIndexDetailReturn()
    {    
        $request = Yii::$app->request;
        $searchModel = new TaPemesananDetailReturnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['id_return'=> null]);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Return",
                    'content'=>$this->renderAjax('index_detail_return', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index_detail_return', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }


    /**
     * Displays a single TaReturn model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> Yii::t('kvgrid','View')." Return  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TaReturn model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TaReturn();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Create New')." Return ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> Yii::t('kvgrid','Create New')." Return ",
                    'content'=>'<span class="text-success">'.Yii::t('kvgrid','Create').' TaReturn '.Yii::t('kvgrid','success').'</span>',
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Create More'),['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> Yii::t('kvgrid','Create New')." Return ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing TaReturn model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Update')." Return  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> Yii::t('kvgrid','View')." Return  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> Yii::t('kvgrid','View')." Return  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing TaReturn model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing TaReturn model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the TaReturn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaReturn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaReturn::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionPilih($id)
    {
        $request = Yii::$app->request;
        $data = TaPemesananDetailReturn::find()->where(['id'=>$id])->one();
        $model = new TaReturn();
        $model->id_pemesanan = $data->id_pemesanan;
        $model->id_supplier = $data->taPemesanan->id_supplier;
        $model->tanggal_pesan = date("Y-m-d");
        $model->id_status = 1;
        $model->no_invoice = $data->taPemesanan->no_invoice;
        $model->save();
        $data->id_return = $model->id;
        $data->save();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }
    public function actionProses($id,$status)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->id_status = $status;
        $model->save();
        if($model->id_status == 100){
            $data = RefObat::find()->where(['id'=> $model->taPemesananDetailReturn->id_obat])->one();
            $data->stok += $model->taPemesananDetailReturn->jumlah;
            $data->save();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }
}

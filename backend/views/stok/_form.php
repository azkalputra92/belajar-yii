<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaStok */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!Yii::$app->request->isAjax) { ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                <?php } ?>

                <?php $form = ActiveForm::begin(); ?>

                <?php // $form->field($model, 'id_obat')->textInput() 
                ?>

                <?php // $form->field($model, 'tanggal')->textInput() 
                ?>

                <?php // $form->field($model, 'id_status_stok')->textInput() 
                ?>

                <?= $form->field($model, 'jumlah_rusak')->textInput() ?>

                <?php // $form->field($model, 'created_at')->textInput() 
                ?>

                <?php // $form->field($model, 'updated_at')->textInput() 
                ?>

                <?php // $form->field($model, 'created_by')->textInput() 
                ?>

                <?php // $form->field($model, 'updated_by')->textInput() 
                ?>


                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Menambahkan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>


                <?php if (!Yii::$app->request->isAjax) { ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
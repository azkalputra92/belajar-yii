<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaStok */

$this->title = Yii::t('kvgrid', 'Add') . ' Stok ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-stok-create">
    <?= $this->render('_form_hilang', [
        'model' => $model,
    ]) ?>
</div>
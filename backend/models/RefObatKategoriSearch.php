<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RefObatKategori;

/**
 * RefObatKategoriSearch represents the model behind the search form about `common\models\RefObatKategori`.
 */
class RefObatKategoriSearch extends RefObatKategori
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_induk'], 'integer'],
            [['kode', 'kategori_obat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefObatKategori::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_induk' => $this->id_induk,
        ]);

        $query->andFilterWhere(['ilike', 'kode', $this->kode])
            ->andFilterWhere(['ilike', 'kategori_obat', $this->kategori_obat]);

        return $dataProvider;
    }
}

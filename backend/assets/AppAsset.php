<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
        'temp_assets/css/font-awesome.css',
        'temp_assets/css/vendors/icofont.css',
        'temp_assets/css/vendors/themify.css',
        'temp_assets/css/vendors/flag-icon.css',
        'temp_assets/css/vendors/feather-icon.css',
        'temp_assets/css/vendors/scrollbar.css',
        // 'temp_assets/css/vendors/bootstrap.css',
        'temp_assets/css/style.css',
        'temp_assets/css/color-1.css',
        'temp_assets/css/responsive.css',
        'css/mystyle.css',
        'css/crud.css'
    ];
    public $js = [
        // 'temp_assets/js/jquery-3.5.1.min.js',
        // 'temp_assets/js/jquery-3.6.0.min.js',
        // 'temp_assets/js/bootstrap/bootstrap.bundle.min.js',
        'temp_assets/js/icons/feather-icon/feather.min.js',
        'temp_assets/js/icons/feather-icon/feather-icon.js',
        'temp_assets/js/scrollbar/simplebar.js',
        'temp_assets/js/scrollbar/custom.js',
        'temp_assets/js/config.js',
        'temp_assets/js/sidebar-menu.js',
        'temp_assets/js/tooltip-init.js',
        'temp_assets/js/script.js',
        // 'temp_assets/js/theme-customizer/customizer.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}

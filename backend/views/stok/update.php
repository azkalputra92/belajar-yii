<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaStok */

$this->title = Yii::t('kvgrid','Update').' Stok ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-stok-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use kartik\grid\GridView;
// use johnitvn\ajaxcrud\CrudAsset; 
use backend\assets\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\icons\Icon;
    
Icon::map($this, Icon::FAS);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaPemesananDetailReturnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile(
    '@web/temp_assets/js/modal_large.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->title = 'Pemesanan Detail Return ';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="row">
<div class="col-sm-12">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable-Pemesanan-Detail-Return',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'pjaxSettings' => [
               'options' => [
                   'enablePushState' => false,
               ]
            ],
            'columns' => require(__DIR__.'/_columns_detail_return.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fas fa-sync-alt"></i> '.Yii::t('kvgrid','Refresh'), [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-light', 'title'=>Yii::t('kvgrid','Reset Grid')])
                    // .'{toggleData}'
                    // .'{export}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="fas fa-list"></i> Data Pemesanan Detail Return ',
                // 'before'=>Html::a('<i class="fas fa-plus"></i> '.Yii::t('kvgrid','Add'), ['create'],
                //             ['role'=>'modal-remote','title'=> Yii::t('kvgrid','Create New Item'),'class'=>'btn btn-primary']),
                'after'=>'<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    // "size" => "modal-lg",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

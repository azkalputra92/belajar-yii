<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaPenjualanDetail */

$this->title = Yii::t('kvgrid','Update').' Penjualan Detail ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-penjualan-detail-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

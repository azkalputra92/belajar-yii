<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\RefObatKategori */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!Yii::$app->request->isAjax){ ?>
<div class="row">
<div class="col-sm-12">
<div class="card">
<div class="card-body">
<?php } ?>

    <?php $form = ActiveForm::begin(); ?>

    
    <?= $form->field($model, 'id_induk')->widget(Select2::classname(), [
        'data' => $induk,
        'language' => 'id',
        'options' => ['placeholder' => 'Pilih No Indux Obat'],
        'pluginOptions' => [
            'allowClear' => true
        ],
        ])->label('No Induk'); 
    ?>

    <?php /* $form->field($model, 'id_induk')->textInput() */?>

    <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kategori_obat')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Menambahkan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    

<?php if (!Yii::$app->request->isAjax){ ?>
</div>
</div>
</div>
</div>
<?php } ?>

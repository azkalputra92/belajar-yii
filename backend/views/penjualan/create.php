<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaPenjualan */

$this->title = Yii::t('kvgrid','Add').' Penjualan ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ta-penjualan-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

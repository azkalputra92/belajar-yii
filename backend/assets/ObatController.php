<?php

namespace backend\controllers;

use Yii;
use common\models\RefObat;
use backend\models\RefObatSearch;
use common\models\RefObatKategori;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * ObatController implements the CRUD actions for RefObat model.
 */
class ObatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefObat models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $request = Yii::$app->request;
        $searchModel = new RefObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Obat  ",
                    'content'=>$this->renderAjax('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"])
                ];    
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }


    /**
     * Displays a single RefObat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> Yii::t('kvgrid','View')." Obat  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new RefObat model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new RefObat(); 
        $kategori = ArrayHelper::map(RefObatKategori::find()->all(), 'id', 'kategori_obat') ;
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Create New')." Obat ",
                    'content'=>$this->renderAjax('create', [
                        'kategori' => $kategori,
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-obat-pjax',
                    'title'=> Yii::t('kvgrid','Create New')." Obat ",
                    'content'=>'<span class="text-success">'.Yii::t('kvgrid','Create').' RefObat '.Yii::t('kvgrid','success').'</span>',
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Create More'),['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> Yii::t('kvgrid','Create New')." Obat ",
                    'content'=>$this->renderAjax('create', [
                        'kategori' => $kategori,
                        'model' => $model,
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'kategori' => $kategori,
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing RefObat model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        $kategori = ArrayHelper::map(RefObatKategori::find()->all(), 'id', 'kategori_obat') ;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> Yii::t('kvgrid','Update')." Obat  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'kategori' => $kategori
                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-obat-pjax',
                    'title'=> Yii::t('kvgrid','View')." Obat  ",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'kategori' => $kategori

                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                            Html::a(Yii::t('kvgrid','Edit'),['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> Yii::t('kvgrid','View')." Obat  ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'kategori' => $kategori

                    ]),
                    'footer'=> Html::button(Yii::t('kvgrid','Close'),['class'=>'btn btn-light pull-left','data-dismiss'=>"modal"]).
                                Html::button(Yii::t('kvgrid','Save'),['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'kategori' => $kategori
                ]);
            }
        }
    }

    /**
     * Delete an existing RefObat model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-obat-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing RefObat model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-obat-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the RefObat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefObat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefObat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

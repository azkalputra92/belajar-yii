<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\RefObat */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (!Yii::$app->request->isAjax){ ?>
<div class="row">
<div class="col-sm-12">
<div class="card">
<div class="card-body">
<?php } ?>

    <?php $form = ActiveForm::begin(); ?>

    <?php //$form->field($model, 'id_supplier')->textInput() ?>

    <?php //$form->field($model, 'id_kategori')->textInput() ?>

    <?= $form->field($model, 'id_kategori')->widget(Select2::classname(), [
            'data' => $kategori,
            'language' => 'id',
            'options' => ['placeholder' => 'Pilih Kategori : '],
            'pluginOptions' => [
                'allowClear' => true
         ],
        ])->label('Kategori'); 
    ?>
    <?php // $form->field($model, 'id_supplier')->textInput() ?>

    <?php // $form->field($model, 'id_kategori')->textInput() ?>

    <?= $form->field($model, 'obat')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'stok')->textInput() ?>

    <?php // $form->field($model, 'harga_pokok_penjualan')->textInput() ?>

    <?php
        // Integer only
        echo '<label class="control-label">Harga Pokok Penjualan</label>';
        echo NumberControl::widget([
            'model' => $model,
            'attribute' => 'harga_pokok_penjualan',
            'maskedInputOptions' => ['digits' => 0, 'prefix' => 'Rp ', 'groupSeparator' => '.','radixPoint' => ','],
        ]);
    ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Menambahkan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    

<?php if (!Yii::$app->request->isAjax){ ?>
</div>
</div>
</div>
</div>
<?php } ?>

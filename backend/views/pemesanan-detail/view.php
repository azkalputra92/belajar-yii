<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaPemesananDetail */
?>
<?php if (!Yii::$app->request->isAjax){ ?>
<div class="row">
<div class="col-sm-12">
<div class="card">
<div class="card-header">
    <?= Html::a(Yii::t('kvgrid','Back To Tabel'),['pemesanan/index'],['class'=>'btn btn-primary'])?>
</div>
<div class="card-body">
<?php } ?>
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_pemesanan',
            'id_obat',
            'harga',
            'jumlah',
            'total',
        ],
    ]) ?>

<?php if (!Yii::$app->request->isAjax){ ?>
</div>
</div>
</div>
</div>
<?php } ?>

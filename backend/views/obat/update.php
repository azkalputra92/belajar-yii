<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RefObat */

$this->title = Yii::t('kvgrid','Update').' Obat ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ref-obat-update">

    <?= $this->render('_form', [
                'kategori' => $kategori,
        'model' => $model,
    ]) ?>

</div>

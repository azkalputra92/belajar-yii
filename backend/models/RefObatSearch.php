<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RefObat;

/**
 * RefObatSearch represents the model behind the search form about `common\models\RefObat`.
 */
class RefObatSearch extends RefObat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_supplier', 'id_kategori', 'stok', 'harga_pokok_penjualan'], 'integer'],
            [['obat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefObat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_supplier' => $this->id_supplier,
            'id_kategori' => $this->id_kategori,
            'stok' => $this->stok,
            'harga_pokok_penjualan' => $this->harga_pokok_penjualan,
        ]);

        $query->andFilterWhere(['ilike', 'obat', $this->obat]);

        return $dataProvider;
    }
}

<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // 'vAlign' => 'middle',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'id_pemesanan',
    //     'vAlign' => 'middle',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Supplier',
        'attribute'=>'refSupplier.supplier',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tanggal_pesan',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Obat',
        'vAlign' => 'middle',
        'value'=>function($model){
            return $model->taPemesananDetailReturn->refObat->obat;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Jumlah',
        'vAlign' => 'middle',
        'value'=>function($model){
            return $model->taPemesananDetailReturn->jumlah;
        }
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'total_harga',
    //     'vAlign' => 'middle',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'id_status',
    //     'vAlign' => 'middle',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'no_resi',
        // 'vAlign' => 'middle',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'no_invoice',
        // 'vAlign' => 'middle',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
        // 'vAlign' => 'middle',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
        // 'vAlign' => 'middle',
    // ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'id'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
    //                       'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'template' => '{proses}',
        'buttons' => [
            "proses" => function ($url, $model, $key) use($status){
                if($status){
                    if($status < 99){
                    $tombol = ($status == 2)?'Sampai':'Selesai';
                    return  Html::a($tombol, ['proses', 'id' => $model->id,'status'=> ($status == 3)?100:$status+1], [
                        'class' => 'btn btn-primary mb-2',
                        'title'=>Yii::t('kvgrid',$tombol), 
                        'role' => 'modal-remote',
                        'data-confirm'=>false, 'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>Yii::t('kvgrid','Ingin Merubah Status Menjadi '.$tombol.' ini?'),
                        'data-confirm-message'=>Yii::t('kvgrid','Ingin Merubah Status Menjadi '.$tombol.' Ini?'),
                    ]);
                    }
                }else{
                    return  Html::a('Proses', ['proses', 'id' => $model->id,'status'=> 2], [
                        'class' => 'btn btn-primary mb-2',
                        'title'=>Yii::t('kvgrid','Proses'), 
                        'role' => 'modal-remote',
                        'data-confirm'=>false, 'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>Yii::t('kvgrid','Ingin Merubah Status Menjadi Proses ini?'),
                        'data-confirm-message'=>Yii::t('kvgrid','Ingin Merubah Status Menjadi Proses Ini?'),
                    ]);
                }
            },
        ],
    ],

];   
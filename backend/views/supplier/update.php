<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RefSupplier */

$this->title = Yii::t('kvgrid','Update').' Supplier ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ref-supplier-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

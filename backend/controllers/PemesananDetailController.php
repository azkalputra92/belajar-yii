<?php

namespace backend\controllers;

use Yii;
use common\models\TaPemesananDetail;
use common\models\TaPemesanan;
use common\models\TaPemesananDetailReturn;
use backend\models\TaPemesananDetailSearch;
use backend\models\RefObatSearch;
use backend\models\RefSupplierObatSearch;
use backend\models\RefSupplierSearch;
use backend\models\TaPemesananSearch;
use common\models\RefObat;
use common\models\RefSupplier;
use common\models\RefSupplierObat;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * PemesananDetailController implements the CRUD actions for TaPemesananDetail model.
 */
class PemesananDetailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaPemesananDetail models.
     * @return mixed
     */
    public function actionIndex($id_pemesanan, $id_supplier)
    {
        $request = Yii::$app->request;
        $searchModel = new TaPemesananDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_pemesanan' => $id_pemesanan]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan Detail  ",
                'content' => $this->renderAjax('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_supplier' => $id_supplier,
                    'id_pemesanan' => $id_pemesanan,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_supplier' => $id_supplier,
                'id_pemesanan' => $id_pemesanan,
            ]);
        }
    }

    public function actionIndexDetail($id_pemesanan)
    {
        $request = Yii::$app->request;
        $searchModel = new TaPemesananDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_pemesanan' => $id_pemesanan]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan Detail  ",
                'content' => $this->renderAjax('index_detail', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_pemesanan' => $id_pemesanan,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_detail', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_pemesanan' => $id_pemesanan,
            ]);
        }
    }
    public function actionIndexDetailSampai($id_pemesanan)
    {
        $request = Yii::$app->request;
        $searchModel = new TaPemesananDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_pemesanan' => $id_pemesanan]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Pemesanan Detail  ",
                'content' => $this->renderAjax('index_detail_sampai', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_pemesanan' => $id_pemesanan,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_detail_sampai', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_pemesanan' => $id_pemesanan,
            ]);
        }
    }


    /**
     * Displays a single TaPemesananDetail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => Yii::t('kvgrid', 'View') . " Pemesanan Detail  ",
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('kvgrid', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TaPemesananDetail model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TaPemesananDetail();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Pemesanan Detail ",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pemesanan-pjax',
                    'title' => Yii::t('kvgrid', 'Create New') . " Pemesanan Detail ",
                    'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' TaPemesananDetail ' . Yii::t('kvgrid', 'success') . '</span>',
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::a(Yii::t('kvgrid', 'Create More'), ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Pemesanan Detail ",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing TaPemesananDetail model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Update') . " Pemesanan Detail  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {

                $model->setHarga();
                $model->setTotalPesanan();

                return [
                    'forceReload' => '#crud-datatable-pemesanan-pjax',
                    'title' => Yii::t('kvgrid', 'View') . " Pemesanan Detail  ",
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::a(Yii::t('kvgrid', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'View') . " Pemesanan Detail  ",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {
                $model->setHarga();
                $model->setTotalPesanan();

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionPesan()
    {
        $request = Yii::$app->request;
        $searchModel = new RefSupplierObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Obat  ",
                'content' => $this->renderAjax('index_pesan', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_pesan', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionObat($id_supplier, $id_pemesanan)
    {
        $request = Yii::$app->request;
        $searchModel = new RefSupplierObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id_supplier' => $id_supplier]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Stok  ",
                'content' => $this->renderAjax('index_obat', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'id_supplier' => $id_supplier,
                    'id_pemesanan' => $id_pemesanan
                ]),
                'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('index_obat', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'id_supplier' => $id_supplier,
                'id_pemesanan' => $id_pemesanan
            ]);
        }
    }

    public function actionPilih($id_obat, $id_pemesanan)
    {
        $request = Yii::$app->request;
        $model = new TaPemesananDetail();
        $model->id_obat = $id_obat;
        $model->id_pemesanan = $id_pemesanan;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Pemesanan Detail ",
                    'content' => $this->renderAjax('pilih', [
                        'model' => $model,

                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $model->setHarga();
                    $model->setTotalPesanan();
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }

                return [
                    'forceReload' => '#crud-datatable-pemesanan-pjax',
                    'title' => Yii::t('kvgrid', 'Create New') . " Pemesanan Detail ",
                    'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' TaPemesananDetail ' . Yii::t('kvgrid', 'success') . '</span>',
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"])
                    // Html::a(Yii::t('kvgrid','Create More'),['obat', 'id_supplier' => $model->id_supplier],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'Create New') . " Pemesanan Detail ",
                    'content' => $this->renderAjax('pilih', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {

                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $model->setHarga();
                    $model->setTotalPesanan();
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }

                return $this->redirect(['index']);
            } else {
                return $this->render('pilih', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing TaPemesananDetail model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $thisModel = $this->findModel($id);
        $modelPemesanan = TaPemesanan::findOne(['id' => $thisModel->id_pemesanan]);
        // $penjualanModel = TaPenjualan::findOne(['id' => $thisModel->id_penjualan]);
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $modelPemesanan->total_harga -= $thisModel->total;
            $modelPemesanan->save();
            // $penjualanModel->save();
            $transaction->commit();
            $thisModel->delete();
            //   $thisModel->setTotalPenjualan();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pemesanan-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing TaPemesananDetail model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the TaPemesananDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaPemesananDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionObatRusak($id)
    {
        $request = Yii::$app->request;
        $TaPemesananDetail = TaPemesananDetail::find()->where(['id'=>$id])->one();
        
        $model = TaPemesananDetailReturn::find()->where(['id_pemesanan' => $TaPemesananDetail->id_pemesanan,'id_obat' => $TaPemesananDetail->id_obat])->one();
        if(!$model){
            $model = new TaPemesananDetailReturn;
        }

        $model->id_pemesanan = $TaPemesananDetail->id_pemesanan;
        $model->id_obat = $TaPemesananDetail->id_obat;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('kvgrid', 'Update') . " Pemesanan Detail  ",
                    'content' => $this->renderAjax('obat_rusak', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pemesanan-pjax',
                    'title' => Yii::t('kvgrid', 'View') . " Pemesanan Detail  ",
                    'content' => '<span class="text-success">' . Yii::t('kvgrid', 'Create') . ' Obat Rusak ' . Yii::t('kvgrid', 'success') . '</span>',
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]).
                                Html::a(Yii::t('kvgrid', 'Detail Obat'), ['index-detail-sampai', 'id_pemesanan' => $model->id_pemesanan],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('kvgrid', 'View') . " Pemesanan Detail  ",
                    'content' => $this->renderAjax('obat_rusak', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('kvgrid', 'Close'), ['class' => 'btn btn-light pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('kvgrid', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('obat_rusak', [
                    'model' => $model,
                ]);
            }
        }
    }
    protected function findModel($id)
    {
        if (($model = TaPemesananDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

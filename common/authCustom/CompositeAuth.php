<?php

namespace common\authCustom;

use Yii;

class CompositeAuth extends \yii\filters\auth\CompositeAuth
{
    public $scope = null;
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $server = Yii::$app->getModule('oauth2')->getServer();
        $server->verifyResourceRequest(null, null, $this->scope);
        
        return parent::beforeAction($action);
    }
}
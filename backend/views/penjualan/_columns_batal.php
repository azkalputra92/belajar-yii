<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
        // 'vAlign' => 'middle',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'no_invoice',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tanggal',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format' => 'currency',
        'attribute'=>'total_harga',
        'vAlign' => 'middle',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'statusPenjualan.status',
        'vAlign' => 'middle',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
        // 'vAlign' => 'middle',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
        // 'vAlign' => 'middle',
    // ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'template'=>'{view}{delete}',
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'id'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','View'),'data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Update'), 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>Yii::t('kvgrid','Delete'), 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>Yii::t('kvgrid','Are you sure?'),
    //                       'data-confirm-message'=>Yii::t('kvgrid','Are you sure want to delete this item?')], 
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Aksi',
        'template' => '{detail}',
        'buttons' => [
            "detail" => function ($url, $model, $key) {
                return  Html::a('Detail', ['penjualan-detail/index-detail', 'id_penjualan' => $model->id], [
                    'class' => 'btn btn-primary mb-2',
                    'title'=>Yii::t('kvgrid','Detail Penjualan'), 
                    //'data-pjax' => '0',
                    'role' => 'modal-remote',
                    'target' => 'modal'
                ]);
            // },
            // "proses" => function ($url, $model, $key) {
            //     return  Html::a('Siapkan', ['penjualan/index-disiapkan'], [
            //         'class' => 'btn btn-success mb-2',
            //         //'data-pjax' => '0',
            //         'role' => 'modal-remote',
            //         'target' => 'modal'
            //     ]);
            // },
            }
        ],
    ],

];   
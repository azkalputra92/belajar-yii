<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RefObatKategori */

$this->title = Yii::t('kvgrid','Update').' Obat Kategori ';
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ref-obat-kategori-update">

    <?= $this->render('_form', [
        'model' => $model,
        'induk' => $induk,
    ]) ?>

</div>
